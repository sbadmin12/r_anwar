<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

$config = array(
    'public_view'=>'usercontrol/index',
    'admin_view'=>'admincontrol/index',
    'admin_level'=>array(
        '3'=>'user_control',
        '6'=>'admin_control',
    ),
    'admin_url'=>'admincontrol/',
);
?>