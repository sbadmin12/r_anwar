<?php
/*
|--------------------------------------------------------------------------
| Image Preset Sizes
|--------------------------------------------------------------------------
|
| Specify the preset sizes you want to use in your code. Only these preset 
| will be accepted by the controller for security.
|
| Each preset exists of a width and height. If one of the dimensions are 
| equal to 0, it will automatically calculate a matching width or height 
| to maintain the original ratio.
|
| If both dimensions are specified it will automatically crop the 
| resulting image so that it fits those dimensions.
|
*/
$config["image_sizes"]["thumb"] = array(84,84);
$config["image_sizes"]["small"]  = array(134, 134);
$config["image_sizes"]["medium"] = array(268, 249);
$config["image_sizes"]["large"]  = array(280, 380);
//$config["image_sizes"]["extra"]  = array(268, 249);


$config["image_sizes"]["slider"]  = array(480,441);
$config["image_sizes"]["view"]  = array(121,57);


$config["image_sizes"]["logo"] = array(139,39);
$config["image_sizes"]["icon"] = array(16,16);