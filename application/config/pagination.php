<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(

	'base_url' =>'',
	
	'total_rows' =>200,
	
	'per_page' =>20,
	
	'uri_segment' =>3,
	
	'num_links' =>2,
	
	'use_page_numbers' => TRUE,
	
	'page_query_string' => TRUE,
	
	'reuse_query_string' => FALSE,
	
	'prefix' =>'',
	
	'suffix' =>'',
	
	'use_global_url_suffix' => FALSE,
	
	'full_tag_open' => '<p>',
	
	'full_tag_close' => '</p>',
	
	'first_links' => 'First',
	
	'first_tag_open' => '<div>',
	
	'first_tag_close' => '</div>',
	
	'first_url' => '',
	
	'last_link' => 'Last',
	
	'last_tag_open' => '<div>',
	
	'last_tag_close' => '</div>',
	
	'next_link' => '&gt;',
	
	'next_tag_open' => '<div>',
	
	'next_tag_close' => '</div>',
	
	'prev_link' => '&lt;',
	
	'prev_tag_open' => '<div>',
	
	'prev_tag_close' => '</div>',
	
	'cur_tag_open' => '<b>',
	
	'cur_tag_close' => '</b>',
	
	'num_tag_open' => '<div>',
	
	'num_tag_close' => '</div>',
	
	'display_pages' => TRUE,
	
	'attributes' => array(
		'rel' => FALSE,
	),
);
?>