<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Kategori_model');
		$this->load->model('Brand_model');
		$this->load->model('Product_model');
		$this->load->library('pagination');
		$this->load->helper('slug');
		$this->load->helper('image');
	}
	public function index()
	{
		$data = array(
			'content'=>'homepage',
			
		);
		$this->load->helper("image");
		$this->load->view('usercontrol/index',$data);
		//$this->load->view('welcome_message');
	}
	public function errorpage(){
		$this->load->view('404_override');
	}
}
