<?php
defined('BASEPATH') OR exit('no direct script access allowed');

class Pembayaran extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Product_model');
		$this->load->model('Kategori_model');
		$this->load->model('Brand_model');
		$this->load->helper('slug');
		$this->load->helper('image');
		$this->load->helper('url');
	}
	public function index(){
		$this->load->helper("image");
		$data  = array(
			'content'=>'viewpembayaran'
		);
		$this->load->view('usercontrol/index',$data);
	}
}
?>