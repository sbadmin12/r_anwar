<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Product extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Product_model');
		$this->load->model('Kategori_model');
		$this->load->model('Brand_model');
		$this->load->library('pagination');
		$this->load->helper('slug');
		$this->load->helper('image');
		$this->load->helper('url');
	}
	public function getCategory(){
		
		if($this->uri->segment(2) || $this->uri->segment(3)){
			$id =  $this->uri->segment(2);
			$category = $this->uri->segment(3);
			$test = $this->Product_model->searchByCategory($id);
			$data = array(
				'content'=>'viewproduct',
				'product'=>$test
			);
			 $this->load->view('usercontrol/index',$data);
		}
	}
	public function getBrand(){
		
		if($this->uri->segment(2) || $this->uri->segment(3)){
			$id =  $this->uri->segment(2);
			$category = $this->uri->segment(3);


			$test = $this->Product_model->searchByBrand($id);
			$data = array(
				'content'=>'viewproduct',
				'product'=>$test
			);
			 $this->load->view('usercontrol/index',$data);
		}
	}
	public function view(){
		if($this->uri->segment(2)){
			$id = $this->uri->segment(2);
			$res =  $this->Product_model->detail($id);
			$data = array(
				'content'=>'detailproduct',
				'data'=>$res
			);
			$this->load->view('usercontrol/index',$data);
		}
	}
	public function all(){
		$res = $this->Product_model->searchAll();
		$data = array(
			'content'=>'viewproduct',
			'product'=>$res
		);
		 $this->load->view('usercontrol/index',$data);
	}
	public function searchproduct(){
		if($this->input->post('query')){
			$query = $this->input->post('query');
			$model = $this->Product_model->searchAll(array(
				//'prodct_category'=>$val,
				'product_name'=>$query,
				'product_description'=>$query,
				'product_unit'=>$query,
			), 'like');
			if($model){
				$data = array(
					'content'=>'viewproduct',
					'product'=>$model
				);
				 $this->load->view('usercontrol/index',$data);
				 unset($_POST['query']);
			}else{
				$data = array(
					'content'=>'viewproduct',
					'message'=>'Produk tidak ditemukan untuk kata kunci '.$query,
				);
				$this->load->view('usercontrol/index',$data);
				unset($_POST['query']);
			}
		}else{
			redirect(base_url('all-products'));
		}
	}
}
?>