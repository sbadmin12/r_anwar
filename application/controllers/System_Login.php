<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class System_Login extends CI_Controller {

    public function __construct(){

        parent::__construct();
        $this->load->library('auth_lib');
        $this->_wrapper = $this->config->item('public_view');
    }
    public function index(){
        $this->form();
    }
    public function form(){
        $this->auth_lib->login($this->_wrapper);
    }
}
?>