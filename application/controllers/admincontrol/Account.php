<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Account extends System_Controller {
	
	var $action = NULL;
	public function __construct(){
		parent::__construct();
		$this->action = $this->_admin_url.strtolower(get_class($this));
	}
	public function index(){
		$this->load->model('Account_model');
		$data = array(
            'content'=>'account',
            'data'=>$this->Account_model->detail(),
            'info'=>array(
                'header'=>'Konfigurasi Akun',
                'map'=>array('Dashboard','Konfigurasi Akun')
            ),
        );
        $config = array(
	        array(
	        	'field' => 'name',
	        	'label' => 'name',
	        	'rules' => 'required'
	        ),
	        array(
	        	'field' => 'email',
	        	'label' => 'email',
	        	'rules' => 'required|valid_email'
	        ),
	    );
	    if($this->input->post('cur_pass') != NULL){
			array_push($config,array(
				'field' => 'pass',
	        	'label' => 'pass',
	        	'rules' => 'required'
			));
			array_push($config,array(
				'field' => 'conf_pass',
	        	'label' => 'conf_pass',
	        	'rules' => 'required|callback_check_pass'
			));
		}
	    
		$this->form_validation->set_rules($config);
		
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			if($this->Account_model->check_pass($this->input->post('cur_pass'))){
				Account_model::$field = $this->input->post();
				if($this->Account_model->ubah()){
					$this->session->set_flashdata('account_msg','Ubah Informasi Akun Berhasil');
					redirect('admincontrol/account');
				}else{
					$this->session->set_flashdata('account_err','try again');
					$this->load->view($this->_admin_view,$data);
				}
			}else{
				$this->session->set_flashdata('account_err','Current Password isn\'t match');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function check_pass($conf_pass){
		if($this->input->post('pass') != $conf_pass){
			$this->form_validation->set_message('check_pass','Confirm Password do not match, please try again');
			log_message('ERROR',print_r($conf_pass,TRUE));
			return FALSE;
		}else{
			return TRUE;
		}
	}
}
?>