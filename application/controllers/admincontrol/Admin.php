<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Admin extends System_Controller {
	
	var $action = NULL;
	public function __construct(){
		
		parent::__construct();
		$this->restrict_level();
		$this->load->model('Account_model');
		$this->load->helper('array');
		$this->action = $this->_admin_url.strtolower(get_class($this));
	}
	public function index(){
		$data = array(
			'content'=>'admin',
			'data'=>$this->Account_model->searchAll(),
			'info'=>array(
				'header'=>'Admin Manajemen',
				'map'=>array('Dashboard','Admin')
			)
		);
		$this->load->view($this->_admin_view,$data);
	}
	public function addAdmin(){
		$data = array(
			'content'=>'admin_form',
			'info'=>array(
				'header'=>'Tambah Admin',
				'map'=>array('Dashboard','Admin','Tambah Admin')
			)
		);
		$config = array(
	        array(
	                'field' => 'name',
	                'label' => 'name',
	                'rules' => 'trim|required'
	        ),
	        array(
	                'field' => 'email',
	                'label' => 'email',
	                'rules' => 'trim|required|valid_email'
	        ),
	        array(
	                'field' => 'pass',
	                'label' => 'pass',
	                'rules' => 'trim|required'
	        ),
	         array(
	                'field' => 'pass',
	                'label' => 'pass',
	                'rules' => 'trim|required|callback_check_pass'
	        ),
	    );
	    
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			Account_model::$field = $this->input->post();
			if($this->Account_model->save()){
				$this->session->set_flashdata('success','Tambah Anggota Berhasil');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','try again');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function ubah($id){
		$data = array(
			'content'=>'admin_form',
			'data'=> $this->Account_model->detail($id),
			'info'=>array(
				'header'=>'Ubah Informasi Admin',
				'map'=>array('Dashboard','Admin','Ubah Informasi')
			)
		);
		$config = array(
	        array(
	                'field' => 'name',
	                'label' => 'name',
	                'rules' => 'trim|required'
	        ),
	        array(
	                'field' => 'email',
	                'label' => 'email',
	                'rules' => 'trim|required|valid_email'
	        ),
	    );
	    
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			Account_model::$field = $this->input->post();
			if($this->Account_model->ubah($id)){
				$this->session->set_flashdata('success','Ubah Informasi Admin Berhasil');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','try again');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function delete($id){
		$this->load->helper('security');
		$id = xss_clean($id);
		$model = $this->Account_model->delete($id);
		if($model){
			$this->session->set_flashdata('success','Hapus Anggota Berhasil');
			redirect($this->action);
		}else{
			$this->session->set_flashdata('error','try again');
			redirect($this->action);
		}
	}
	public function check_pass($conf_pass){
		if($this->input->post('pass') != $conf_pass){
			$this->form_validation->set_message('check_pass','Confirm Password do not match, please try again');
			log_message('ERROR',print_r($conf_pass,TRUE));
			return FALSE;
		}else{
			return TRUE;
		}
	}
	
}
?>