<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Brand extends System_Controller {
	
	var $action = NULL;
	public function __construct(){
		parent::__construct();
		$this->load->helper('security');
		$this->load->model('Brand_model');
		$this->action = $this->_admin_url.strtolower(get_class($this));
	}
	public function index(){
		$data = array(
			'content'=>'brand',
			'data'=>$this->Brand_model->searchAll(),
			'info'=> array(
				'header'=>'Brand',
				'map'=>array('Dashboard','Brand'),
			),
		);
		$this->load->view($this->_admin_view, $data);
	}
	public function addbrand(){
		$data = array(
			'content'=>'brand_form',
			'info' => array(
				'header'=>'Brand',
				'map'=>array('Dashboard','Brand','Tambah Brand')
			)
		);
		$config = array(
	        array(
	                'field' => 'product_brand',
	                'label' => 'product_brand',
	                'rules' => 'required'
	        ),
	    );
	    $this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			Brand_model::$field = $this->input->post();
			if($this->Brand_model->save()){
				$this->session->set_flashdata('success','Tambah Kategori Berhasil');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','try again');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function update($id){
		$id = xss_clean($id);
		$data = array(
			'content'=>'brand_form',
			'data'=> $this->Brand_model->detail($id),
			'info' => array(
				'header'=>'Kategori',
				'map'=>array('Dashboard','Kategori','Ubah Kategori')
			)
		);
		$config = array(
	        array(
	                'field' => 'product_brand',
	                'label' => 'product_brand',
	                'rules' => 'required'
	        ),
	    );
	    
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			Brand_model::$field = $this->input->post();
			if($this->Brand_model->update($id)){
				$this->session->set_flashdata('success','add category success');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','try again');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function delete($id){
		$id = xss_clean($id);
		$model = $this->Brand_model->delete($id);
		if($model){
			$this->session->set_flashdata('success','Hapus Kategori Berhasil');
			redirect($this->action);
		}else{
			$this->session->set_flashdata('error','try again');
			redirect($this->action);
		}
	}
}
?>