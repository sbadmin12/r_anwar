<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Dashboard extends System_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
    }
    public function index(){
        $data = array(
            'content'=>'home',
            'info'=>array(
                'header'=>'Dashboard',
                'map'=>array('Dashboard')
            ),
        );
        $this->load->view($this->_admin_view,$data);
    }
    
    public function logout(){
        $this->auth_lib->logout();
    }
}