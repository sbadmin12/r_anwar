<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Kategori extends System_Controller {
	
	var $action = NULL;
	public function __construct(){
		parent::__construct();
		$this->load->helper('security');
		$this->load->model('Kategori_model');
		$this->action = $this->_admin_url.strtolower(get_class($this));
	}
	public function index(){
		$data = array(
			'content'=>'kategori',
			'data'=>$this->Kategori_model->searchAll(),
			'info'=> array(
				'header'=>'Kategori',
				'map'=>array('Dashboard','Kategori'),
			),
		);
		$this->load->view($this->_admin_view, $data);
	}
	public function addkategori(){
		$data = array(
			'content'=>'kategori_form',
			'parent'=>$this->Kategori_model->searchAll(array('parent_id'=>'0')),
			'info' => array(
				'header'=>'Kategori',
				'map'=>array('Dashboard','Kategori','Tambah Kategori')
			)
		);
		$config = array(
	        array(
	                'field' => 'product_category',
	                'label' => 'product_category',
	                'rules' => 'required'
	        ),
	    );
	    $this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			Kategori_model::$field = $this->input->post();
			if($this->Kategori_model->save()){
				$this->session->set_flashdata('success','Tambah Kategori Berhasil');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','try again');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function update($id){
		$id = xss_clean($id);
		$data = array(
			'content'=>'kategori_form',
			'data'=> $this->Kategori_model->detail($id),
			'parent'=>$this->Kategori_model->searchAll(array('parent_id'=>'0','id !='=>$id)),
			'info' => array(
				'header'=>'Kategori',
				'map'=>array('Dashboard','Kategori','Ubah Kategori')
			)
		);
		$config = array(
	        array(
	                'field' => 'product_category',
	                'label' => 'product_category',
	                'rules' => 'required'
	        ),
	    );
	    
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			Kategori_model::$field = $this->input->post();
			if($this->Kategori_model->update($id)){
				$this->session->set_flashdata('success','add category success');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','try again');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function delete($id){
		$id = xss_clean($id);
		$model = $this->Kategori_model->delete($id);
		if($model){
			$this->session->set_flashdata('success','Hapus Kategori Berhasil');
			redirect($this->action);
		}else{
			$this->session->set_flashdata('error','try again');
			redirect($this->action);
		}
	}
	public function updatetop(){
		if($this->input->post('catID')){
			extract($this->input->post());
			Kategori_model::$field['top'] = $select;
			if($this->Kategori_model->update($catID)){
				exit(json_encode(array('success'=>TRUE)));
			}
		}
	}
}
?>