<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Product extends System_Controller {
	var $model = NULL;
	var $action = NULL;
	public function __construct(){
		parent::__construct();
		$this->load->model('Kategori_model');
		$this->load->model('Brand_model');
		$this->load->model('Product_model');
		$this->load->helper("image");
		$this->load->helper('security');
		$this->load->helper('url');
		$this->action = $this->_admin_url.strtolower(get_class($this));
	}
	public function index(){
		$this->model = $this->Product_model->searchAll();
		$this->listView();
	}
	public function listView(){
		$data = array(
			'content'=>'product',
			'data'=>$this->model,
			'info' => array(
				'header'=>'Barang',
				'map'=>array('Dashboard','Barang')
			)
		);
		$this->load->view($this->_admin_view,$data);
	}
	public function addproduct(){
		$data = array(
			'content'=>'product_form',
			'info' => array(
				'header'=>'Barang',
				'map'=>array('Dashboard','Barang','Tambah Barang')
			)
		);
		$config = array(
	        array(
	                'field' => 'product_name',
	                'label' => 'product_name',
	                'rules' => 'required'
	        ),
	        array(
	                'field' => 'product_category',
	                'label' => 'product_category',
	                'rules' => 'required',
	                'errors' => array(
	                        'required' => 'You must provide a %s.',
	                ),
	        ),
	       /* array(
	                'field' => 'product_price',
	                'label' => 'product_price',
	                'rules' => 'integer'
	        ),*/
	        array(
	                'field' => 'product_image',
	                'label' => 'product_image',
	                'rules' => 'required'
	        ),
		);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			Product_model::$field = $this->input->post();
			if($this->Product_model->save()){
				$this->session->set_flashdata('success','add product success');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','try again');
				
				$this->load->view($this->_admin_view,$data);
			}
		}	
	}
	public function update($id=''){
		$id = xss_clean($id);
		$data = array(
			'content'=>'product_form',
			'data'=> $this->Product_model->detail($id),
			'info' => array(
				'header'=>'Barang',
				'map'=>array('Dashboard','Barang','Ubah Barang')
			)
		);
		$config = array(
	        array(
	                'field' => 'product_name',
	                'label' => 'product_name',
	                'rules' => 'required'
	        ),
	        array(
	                'field' => 'product_category',
	                'label' => 'product_category',
	                'rules' => 'required',
	                'errors' => array(
	                        'required' => 'You must provide a %s.',
	                ),
	        ),
	        /*array(
	                'field' => 'product_price',
	                'label' => 'product_price',
	                'rules' => 'integer'
	        ),*/
	        array(
	                'field' => 'product_image',
	                'label' => 'product_image',
	                'rules' => 'required'
	        ),
		);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);
		}else{
			Product_model::$field = $this->input->post();
			$detail = $this->Product_model->detail($id);
			if($this->Product_model->update($id)){
				$this->session->set_flashdata('success','edit barang berhasil');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','gagal edit barang');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function delete($id){
		$data = array(
			'content'=>'product_form',
		);
		$this->model =  $this->Product_model->delete($id);
		$id = xss_clean($id);
		
		if($this->model){
			$this->session->set_flashdata('success','barang berhasil di hapus');
			redirect($this->action);
		}else{
			$this->session->set_flashdata('error','gagal menghapus barang');
			$this->load->view($this->_admin_view,$data);
		}
	}
	public function search(){
		//log_message('ERROR',print_r($this->input->post(),TRUE));
		if($this->input->post('searchVal')){
			$val = $this->input->post('searchVal');
			$this->model = $this->Product_model->searchAll(array(
				//'prodct_category'=>$val,
				'product_name'=>$val,
				'product_description'=>$val,
				'product_unit'=>$val,
			), 'like');
			if($this->model){
				$this->listView();
				unset($_POST['searchVal']);
			}else{
				$this->model = $this->Product_model->searchAll();
				$this->session->set_flashdata('error','tidak ada hasil pencarian untuk kata kunci <b>'.$val.'</b>');
				$this->listView();
				unset($_POST['searchVal']);
				unset($_SESSION['error']);
			}
		}else{
			$this->model = $this->Product_model->searchAll();
			$this->listView();
			unset($_POST['searchVal']);
		}
	}
	
}
?>