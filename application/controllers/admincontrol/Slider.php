<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Slider extends System_Controller {
	var $action = NULL;
	var $model = NULL;
	public function __construct(){
		parent::__construct();
		$this->load->helper("image");
		$this->load->helper('security');
		$this->load->model('Slider_model');
		$this->model = $this->Slider_model->searchAll();
		$this->action = $this->_admin_url.strtolower(get_class($this));
	}
	public function index(){
		$data = array(
			'content'=>'slider',
			'data'=>$this->model,
			'info' => array(
				'header'=>'Slider',
				'map'=>array('Dashboard','Slider')
			)
		);
		$this->load->view('admincontrol/index',$data);
	}
	public function addslider(){
		$data = array(
			'content'=>'slider_form',
			'model'=>$this->model,
			'info' => array(
				'header'=>'Slider',
				'map'=>array('Dashboard','Slider','Tambah Slider')
			)
		);
		$config = array(
	        array(
	                'field' => 'image',
	                'label' => 'image',
	                'rules' => 'required'
	        ),
	        array(
	                'field' => 'positions',
	                'label' => 'positions',
	                'rules' => 'required',
	                'errors' => array(
	                        'required' => 'You must provide a %s.',
	                ),
	        ),
		);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view('admincontrol/index',$data);
		}else{
			Slider_model::$field = $this->input->post();
			if($this->Slider_model->save()){
				$this->session->set_flashdata('success','Gambar Slider Berhasil ditambah');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','try again');
				
				$this->load->view($this->_admin_view,$data);
			}
		}	
	}
	public function update($id=''){
		$id = xss_clean($id);
		$data = array(
			'content'=>'slider_form',
			'data'=>$this->Slider_model->detail($id),
			'model'=>$this->model,
			'info' => array(
				'header'=>'Slider',
				'map'=>array('Dashboard','Slider','Ubah Slider')
			)
		);
		$config = array(
	        array(
	                'field' => 'image',
	                'label' => 'image',
	                'rules' => 'required'
	        ),
	        array(
	                'field' => 'positions',
	                'label' => 'positions',
	                'rules' => 'required',
	                'errors' => array(
	                        'required' => 'You must provide a %s.',
	                ),
	        ),
		);
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() == FALSE){
			$this->load->view('admincontrol/index',$data);
		}else{	
			Slider_model::$field = $this->input->post();
			$detail = $this->Slider_model->detail($id);
			if($this->Slider_model->update($id)){
				$this->session->set_flashdata('success','edit gambar berhasil');
				redirect($this->action);
			}else{
				$this->session->set_flashdata('error','gagal edit gambar');
				$this->load->view($this->_admin_view,$data);
			}
		}
	}
	public function delete($id){
		$data = array(
			'content'=>'product_form',
		);
		$this->load->helper('security');
		$id = xss_clean($id);
		$model = $this->Slider_model->delete($id);
		if($model){
			$this->session->set_flashdata('success','barang berhasil di hapus');
			redirect($this->action);
		}else{
			$this->session->set_flashdata('error','gagal menghapus barang');
			$this->load->view($this->_admin_view,$data);
		}
	}
}
?>