<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Upload extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('date','string'));
		
	}
	public function image(){
		
		/*****************************************
		* extract all information from POST data *
		* @Filename                              *
		* @upload_form                           *
		* @token                                 *
		* @timestamp = time()                    *
		* @Upload = Submit Qeury                 *
		* Object Name = 'image'                  *
		******************************************/
		extract($this->input->post());
		
		//create date string format
		$format = '%y-%m-%d_%h-%i-%a';
		$date = mdate($format, time());
		
		//get random string for make sure no one of image can be duplucated
		$rand = random_string('alnum', 5);
		
		//get escaped image name
		$info = pathinfo($Filename);
		$escapedName = $this->db->escape_str($info['basename']);
		
		//verifying token session
		$verifyToken = md5('unique_salt'.$timestamp);
		if($_FILES && $verifyToken === $token){
			//begin upload image
			$response = NULL;
			$config = array(
				'upload_path' => realpath(FCPATH.DIRECTORY_SEPARATOR.'uploads'),
				'file_name' => $date .'_'. $rand .'_'. $escapedName,
				'allowed_types' => '*',
				//'max_size' => '3000',
				'remove_spaces' => TRUE,
			);
			//initialize
			$this->load->library('upload');
			$this->upload->initialize($config);
			//do upload
			if(!$this->upload->do_upload('image')){
				// if error uploading image send to the log file
				if(ENVIRONMENT === 'development'){
					log_message('ERROR','this error from Upload Class');
					log_message('ERROR',print_r($this->upload->display_errors(), TRUE));
				}
			}else{
				$response = $this->upload->data();
			}
			exit(json_encode($response));
		}
	}
}