<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
class Webconfig extends System_Controller {
	var $action = NULL;
	public function __construct(){
		parent::__construct();
		$this->action = $this->_admin_url.strtolower(get_class($this));
	}
	public function index(){
		$this->load->model('Webconfig_model');
		$data = array(
			'res' => Webconfig_model::$field,
			'content' => 'webconfig',
			'info'=> array(
				'header'=>'Konfigurasi Website',
				'map'=>array('Dashboard','Konfigurasi Website')
			),
		);
		$this->form_validation->set_rules('web_email', 'web_email','trim|valid_email|required');
		$this->form_validation->set_message(array('valid_email'=>'enter a valid email address','required'=> 'field %s is required'));
		if ($this->form_validation->run() == FALSE){
			$this->load->view($this->_admin_view,$data);	
		}else{
			Webconfig_model::$field = $this->input->post();
			if($this->Webconfig_model->save()){
				$this->session->set_flashdata('success','Config successfully changed');
				
			}else{
				$this->session->set_flashdata('error','try again');
			}
			redirect($this->action);
			
		}
	}
}

?>