<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class System_Controller extends CI_Controller {

    var $_admin_view = "";
	var $_admin_url = "";
	var $_auth = FALSE;
    public function __construct(){

        parent::__construct();
        $this->load->library('auth_lib');
        $this->load->helper('array');
        $this->auth_lib->restrict();
        $this->_admin_view = $this->config->item('admin_view');
        $this->_admin_url = base_url().$this->config->item('admin_url');
        $this->_auth = $this->check_level();
    }
    function restrict_level(){
        $this->auth_lib->restrict_level();
    }
    public function check_level(){
        return $this->auth_lib->check_level();
    }
}
?>