<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class System_model extends CI_Model {
	
	public static $field =  array();
	public $table = NULL;
	public $id = NULL;
	public function __construct(){
		parent::__construct();
		$helper = array(
			'array',
			'image',
		);
		$this->load->helper($helper);
	}
	public function actionSave(){
		//$this->actionFieldProtect();
		$res = TRUE;
		$query = $this->db->insert($this->table, System_model::$field);
		if(empty($query) && $query != 1){
			$res = FALSE;
		}
		return $res;
	}
	public function actionUpdate(){
		//$this->actionFieldProtect();
		$res = TRUE;
		$this->db->where('id',$this->id);
		$query = $this->db->update($this->table, System_model::$field);
		if(empty($query) && $query != 1){
			$res = FALSE;
		}
		return $res;
	}
	public function actionDelete(){
		$query = $this->db->delete($this->table, array('id' => $this->db->escape_str($this->id))); 
		if($query){
			return TRUE;
		}
	}
	public function actionSearchAll($order = array(), $limit = NULL){
		$this->db->from($this->table);
		if($order != NULL){
			$this->db->order_by($order[0], $order[1]);
		}
		if($limit != NULL){
			$this->db->limit($limit);
		}
		$query = $this->db->get();
		return $query;
	}
	
	public function actionSearchWhere($where = array(), $method = NULL, $order = array(), $limit = NULL){
		$i = 0;
		$this->db->from($this->table);
		if($where){
			if($method === NULL || $method == "and"){
				$this->db->where($where);
			}else if($method == "or"){
				foreach($where as $field=>$record){
					if($i == 0){
						$this->db->where($field,$record);
					}else{
						$this->db->or_where($field, $record);
					}
					$i++;
				}
			}else if($method == "like"){
				foreach($where as $field=>$record){
					if($i == 0){
						$this->db->like($field,$record, 'both');
					}else{
						$this->db->or_like($field, $record, 'both');
					}
					$i++;
				}
			}
			/*foreach ($where as $field=>$record){
				if($method == NULL || $method == 'and'){
					$this->db->where($field,$record);
				}else if($method == 'or'){
					if($i == 0){
						$this->db->where($field, $record);
					}else{
						$this->db->or_where($field, $record);
					}
					$i++;
				}
			}*/
		}
		if($order != NULL){
			$this->db->order_by($order[0], $order[1]);
		}
		if($limit != NULL){
			$this->db->limit($limit);
		}
		$query = $this->db->get();
		return $query;
	}
	public function actionDetail(){
		$this->db->from($this->table);
		if($this->id != 0){
			$this->db->where('id',$this->db->escape_str($this->id));
		}
		$query = $this->db->get();
		return $query;
	}
	public function actionDeleteImage($image){
		if($image){
			$original = realpath(FCPATH.DIRECTORY_SEPARATOR.'uploads') . DIRECTORY_SEPARATOR . $image;
			if(file_exists($original)){
				unlink($original);
			}
			return TRUE;
		}
	}
	public function actionClearFix(){
		System_model::$field = array();
		$this->table = NULL;
		$this->id = NULL;
	}
	protected function actionFieldProtect(){
		$str = array();
		foreach(System_model::$field as $key=>$val){
			$str[$this->db->escape_str($key)] = $this->db->escape_str($val);
		}
		System_model::$field = $str;
	}
}
?>