<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Auth_lib {

    var $CI = NULL;
    var $_valid = NULL;
    var $_wrapper = NULL;
    public function __construct(){

        $this->CI =& get_instance();

        $this->CI->load->database();
        $this->CI->load->library(array('session','form_validation'));
        $this->_valid = $this->CI->form_validation;

        //public view
        $this->_wrapper = $this->CI->config->item('public_view');
    }

    public function login($page=""){

        $this->restrict(TRUE);
        if($page == ""){
            $page = $this->_wrapper;
        }
		$data = array(
            'content'=>'login'
        );
        $config = array(
            array(
                'field'=>'username',
                'label'=>'username_label',
                'rules'=>'trim|required',
            ),
            array(
                'field'=>'pass',
                'label'=>'pass_label',
                'rules'=>'trim|required',
            ),
        );

        $this->_valid->set_rules($config);
        if($this->_valid->run() == FALSE AND $this->CI->input->post('submit_login') ==FALSE){
        	$this->CI->load->view($page, $data);
        }else{
			$login = array(
                'username'=>$this->CI->input->post('username',TRUE),
                'pass'=>$this->CI->input->post('pass',TRUE),
            );
            if($this->_validate_login($login)){
                $this->redirect();
            }else{
                $this->CI->session->set_flashdata('err','Gagal Login');
                redirect('sbadmin');
            }
		}
    }
    public function _validate_login($login = NULL){

        $this->CI->load->helper('array');
        if(!isset($login) && ! is_array($login)){
            return FALSE;
        }
        if(count($login) != 2){
            return FALSE;
        }
        $query = $this->CI->db->get_where('admincontrol',array(
            'username'=>$this->CI->db->escape_str($login['username']),
            'pass'=>md5($this->CI->db->escape_str($login['pass']))
        ));
        if($query->num_rows() == 1){

            foreach($query->result() as $val){

                $admin = $val->name;
                $level = $val->level;
                $id = $val->id;
            }
            $this->CI->session->set_userdata(array(
                'logged_user'=>$admin,
                'logged_user_level'=> element($level,$this->CI->config->item('admin_level')),
                'logged_user_id'=> $id,
            ));
            return TRUE;
        }else{
            return FALSE;
        }
    }
    public function logged_in(){

        if($this->CI->session->userdata('logged_user') == ""){
            return FALSE;
        }else{
            return TRUE;
        }
    }
    public function logout(){

        $this->CI->session->sess_destroy();
        redirect('sbadmin');
    }
    public function redirect(){

        if($this->CI->session->userdata('redirector') == ""){
            redirect('admincontrol/dashboard');
        }else{
            redirect($this->CI->session->userdata('redirector'));
        }
    }
    public function restrict($logged_out = FALSE){

        if($logged_out AND $this->logged_in()){
            $this->redirect();
        }
        if(! $logged_out AND ! $this->logged_in()){
            $this->CI->session->set_userdata('redirector',$this->CI->uri->uri_string());
            redirect('sbadmin','location');
        }
    }
    public function restrict_level(){

        if( FALSE == $this->check_level()){
            $this->redirect();
        }
    }
    public function check_level(){
		$level = "admin_control";
        $cookie = $this->CI->session->userdata('logged_user_level');
        if($cookie != $level){
            return FALSE;
        }else{
            return TRUE;
        }
    }
}
?>