<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Account_model extends System_model {
	public static $field = array();
	
	public function __construct(){
		parent::__construct();
		$this->actionClearFix();
		$this->table = 'admincontrol';
	}
	public function save(){
		$this->beforeSave();
		System_model::$field = Account_model::$field;
		return $this->actionSave();
	}
	public function ubah(){
		$this->beforeSave();
		System_model::$field = Account_model::$field;
		return $this->actionUpdate();
	}
	public function delete($id){
		$this->id = $id;
		return $this->actionDelete();
	}
	public function detail($id=NULL){
		if($id == NULL){
			$this->id = $this->session->userdata('logged_user_id');
		}else{
			$this->id = $id;
		}
		$query = $this->actionDetail();
		if($query){
			return $query->row();
		}
	}
	public function searchAll($field = NULL, $record = NULL){
		if($field != NULL && $record != NULL){
			$query = $this->actionSearchWhere(array($field=>$record));
			if($query){
				return $query->result();
			}	
		}else if($field == NULL && $record == NULL){
			$query = $this->actionSearchAll();
			if($query){
				return $query->result();
			}
		}
	}
	public function check_pass($cur_pass){
		$query = $this->detail();
		if($query->pass == md5($cur_pass) || $cur_pass == ""){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function beforeSave(){
		$level = $this->detail();
		Account_model::$field['username'] = $level->username;
		Account_model::$field['level'] = $level->level;
		if(Account_model::$field['pass'] == NULL){
			Account_model::$field = elements(array('name','email','level'),Account_model::$field);
		}else{
			Account_model::$field = elements(array('name','email','username','pass','level'),Account_model::$field);
			Account_model::$field['pass'] = md5(Account_model::$field['pass']);
		}	
	}
}
?>