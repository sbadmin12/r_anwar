<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Brand_model extends System_model {
	
	public static $field = array();
	
	public function __construct(){
		parent::__construct();
		$this->actionClearFix();
		$this->table = 'brand';
	}
	public function save(){
		System_model::$field = Brand_model::$field;
		$this->beforeSave();
		return $this->actionSave();
	}
	public function update($id){
		$this->id = $id;
		System_model::$field = Brand_model::$field;
		return $this->actionUpdate();
	}
	public function delete($id){
		$this->id = $id;
		return $this->actionDelete();
	}
	public function detail($id=0){
		$this->id =  $id;
		$query = $this->actionDetail();
		if($query){
			return $query->row();
		}
	}
	public function searchAll($where = array()){
		if($where){
			$query = $this->actionSearchWhere($where,NULL,array('positions','ASC'));
			if($query){
				return $query->result();
				
			}	
		}else{
			$query = $this->actionSearchAll(array('positions','ASC'));
			if($query){
				return $query->result();
			}
		}
	}
	public function beforeSave(){
		if(System_model::$field['positions'] == NULL){
			$model = $this->actionSearchAll();
			if($model){
				$count = $model->num_rows();
				System_model::$field['positions'] = $count;
			}
		}
	}
}
?>