<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Product_model extends System_model {
	public static $field = array();
	
	public function __construct(){
		parent::__construct();
		$this->actionClearFix();
		$this->table = 'product';
	}
	public function save(){
		System_model::$field = Product_model::$field;
		$this->beforeSave();
		return $this->actionSave();
	}
	public function update($id){
		$this->id = $id;
		$detail =  $this->actionDetail();
		if($detail){
			$res = $detail->row();
			if($res->product_image != Product_model::$field['product_image']){
				$this->actionDeleteImage($res->product_image);
			}
		}
		System_model::$field = Product_model::$field;
		$this->beforeSave();
		return $this->actionUpdate();
	}
	public function delete($id){
		$this->id = $id;
		$detail =  $this->actionDetail();
		if($detail){
			$res = $detail->row();
			$this->actionDeleteImage($res->product_image);
		}
		return $this->actionDelete();
	}
	public function searchAll($where = array(),$method = NULL, $order = array(), $limit = NULL, $count = FALSE){
		$this->db->select('id, product_name, product_price, product_unit, product_image, positions, product_description');
		$this->db->select('(SELECT category.product_category FROM category WHERE category.id=product.product_category) AS product_category', FALSE);
		if($where){
			$query = $this->actionSearchWhere($where, $method, $order, $limit);
			if($query){
				if($count === TRUE){
					return $query->num_rows();
				}else{
					return $query->result();
				}
			}	
		}else{
			$query = $this->actionSearchAll($order, $limit);
			if($query){
				if($count === TRUE){
					return $query->num_rows();
				}else{
					return $query->result();
				}
			}
		}
	}
	public function searchByCategory($id=0){
		if($id !=0){
			$query = $this->actionSearchWhere(array('product_category'=>$this->db->escape_str($id)));
			if($query){
				return $query->result();
			}
		}
	}
	public function searchByBrand($id=0){
		if($id !=0){
			$query = $this->actionSearchWhere(array('product_brand'=>$this->db->escape_str($id)));
			if($query){
				return $query->result();
			}
		}
	}
	public function detail($id=0){
		$this->id =  $id;
		$query = $this->actionDetail();
		if($query){
			return $query->row();
		}
	}
	public function beforeSave(){
		if(System_model::$field['positions'] == NULL){
			$model = $this->actionSearchAll();
			if($model){
				$count = $model->num_rows();
				System_model::$field['positions'] = $count;
			}
		}
	}
}
?>