<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

class Slider_model extends System_model {
	public static $field = array();
	
	public function __construct(){
		parent::__construct();
		$this->actionClearFix();
		$this->table = 'slider';
	}
	public function save(){
		System_model::$field = Slider_model::$field;
		return $this->actionSave();
	}
	public function update($id){
		$this->id = $id;
		$detail =  $this->actionDetail();
		if($detail){
			$res = $detail->row();
			if($res->image != Slider_model::$field['image']){
				$this->actionDeleteImage($res->image);
			}
		}
		System_model::$field = Slider_model::$field;
		return $this->actionUpdate();
	}
	public function searchAll($field = NULL, $record = NULL){
		if($field != NULL && $record != NULL){
			$query = $this->actionSearchWhere(array($field=>$record));
			if($query){
				return $query->result();
			}	
		}else if($field == NULL && $record == NULL){
			$query = $this->actionSearchAll();
			if($query){
				return $query->result();
			}
		}
	}
	public function detail($id=0){
		$this->id =  $id;
		$query = $this->actionDetail();
		if($query){
			return $query->row();
		}
	}
	public function delete($id){
		$this->id = $id;
		$detail =  $this->actionDetail();
		if($detail){
			$res = $detail->row();
			$this->actionDeleteImage($res->image);
		}
		return $this->actionDelete();
	}
}
?>