<?php
defined('BASEPATH') OR exit ("no direct script access allowed");

class Webconfig_model extends System_model {
	
	public static $field =  array();
	
	public function __construct(){
		parent::__construct();
		$this->table = 'web_config';
		$query = $this->actionSearchAll();
		if($query){
			foreach($query->result() as $d){
				Webconfig_model::$field[$d->name] = $d->val;
			}
		}
	}
	public function save(){
		$result = TRUE;
		$query = $this->actionSearchWhere(array('name'=>'web_logo'));
		if($query){
			$res = $query->row();
			if($res->val != Webconfig_model::$field['web_logo']){
				$this->actionDeleteImage($res->val);
			}
		}
		foreach(Webconfig_model::$field as $d=>$c){
			$this->db->where('name',$d);
			$query = $this->db->update($this->table, array('val'=>$c));
			if(empty($query) && $query != 1){
				$result = FALSE;
			}
		}
		return $result;
	}
	/*public $table = 'web_config';
	
	public function __construct(){
		$this->load->helper('image');
		$this->db->from($this->table);
		$query = $this->db->get();
		foreach($query->result() as $d){
			//p($d);
			Webconfig_model::$field[$d->name] = $d->val;
		}
	}
	public function save(){
		$res = TRUE;
		$query  = $this->db->get_where($this->table,array('name'=>'web_logo'))->row();
		if($query){
			if($query->val != Webconfig_model::$field['web_logo']){
				$thumb = get_url($query->val);
				$real_image = str_replace(base_url().'uploads/_thumbs/','uploads/',image($query->foto_buku,'','',TRUE));
				if(file_exists($real_image)){
					unlink($real_image);
				}
				for($i=0; $i<count($thumb); $i++){
					if(file_exists($thumb[$i])){
						unlink($thumb[$i]);
					}
				}
			}
		}
		foreach(Webconfig_model::$field as $d=>$c){
			$this->db->where('name',$d);
			$query = $this->db->update($this->table, array('val'=>$c));
			if(empty($query) && $query != 1){
				$res = FALSE;
			}
		}
		return $res;
	}*/
}
?>