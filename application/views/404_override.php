<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="keywords" content="<?php  $query =  $this->db->get_where('web_config', array('name' => 'web_keywords'))->row(); echo $query->val; ?>">
	    <meta name="description" content="<?php  $query =  $this->db->get_where('web_config', array('name' => 'web_description'))->row(); echo $query->val; ?>">
	    <meta name="author" content="">

	    <title> <?php  $query =  $this->db->get_where('web_config', array('name' => 'web_title'))->row(); echo $query->val; ?> </title>

		<!-- Icon Website -->
		<link rel="shortcut icon" href="<?php $query =  $this->db->get_where('web_config', array('name' => 'web_logo'))->row(); ?>"/>
		 <?php
    	$source = array(
    		base_url().'assets/css/bootstrap.css',
			base_url().'assets/font-awesome/css/font-awesome.min.css',
			base_url().'assets/css/prettyPhoto.css',
			base_url().'assets/css/animate.css',
			base_url().'assets/css/main.css',
			base_url().'assets/css/responsive.css',
			//base_url().'assets/css/style.css',
    	);
    	for($i=0; $i<count($source); $i++){
			echo link_tag($source[$i],'stylesheet','text/css','','',TRUE);
		}
    ?>
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

	</head>
	
	<body>
		<div class="container text-center">
			<div class="logo" style="margin-top: 50px;">
				<?php  $query =  $this->db->get_where('web_config', array('name' => 'web_name'))->row(); echo $query->val; ?>
			</div>
			<div class="content-404">
				<img src="<?= base_url('assets/images/404.png') ?>" class="img-responsive" alt="" />
				<h1><b>OPPS!</b> We Couldn�t Find this Page</h1>
				<p>Uh... So it looks like you brock something. The page you are looking for has up and Vanished.</p>
				<h2><a href="#">Bring me back Home</a></h2>
			</div>
		</div>

	  
	    <script src="js/jquery.js"></script>
		<script src="js/price-range.js"></script>
	    <script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	    <script src="js/jquery.prettyPhoto.js"></script>
	    <script src="js/main.js"></script>
	</body>
</html>