<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
if($this->session->flashdata('success')){
	echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
	unset($_SESSION['success']);
}
if($this->session->flashdata('error')){
	echo '<div class="alert alert-warning">'.$this->session->flashdata('error').'</div>';
	unset($_SESSION['error']);
}
echo anchor($this->action.'/addadmin', '<button class="btn btn-default create">Tambah Admin</button>');
?>
<div class="table-responsive">
	<table class="table table-condensed table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th> Nama Akun </th>
				<th> Email </th>
				<th> Level </th>
				<th> Tindakan </th>
			</tr>
		</thead>
		<tbody>
<?php
if(isset($data)){
	$i=0;
	foreach($data as $d){
		$i++;
?>
			<tr>
				<td><?php echo $i ?></td>
				<td> <?php echo $d->name ?> </td>
				<td> <?php echo $d->email ?> </td>
				<td> <?php echo element($d->level,$this->config->item('admin_level')) ?> </td>
				<td>
				<?php 
					echo anchor($this->action.'/ubah/'.$d->id,'<button class="btn btn-primary update">Ubah</button>'); 
					echo anchor($this->action.'/delete/'.$d->id,'<button class="btn btn-danger delete">Hapus</button>');?>
				</td>
			</tr>
<?php
	}
}
?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".delete").click(function(e){
			var conf = confirm('Yakin ingin menghapus data ?');
			if(!conf){
				e.preventDefault();
			}
		});
		if($(".alert").length != 0){
			$(".alert").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".alert").remove()});
		}
	});
</script>