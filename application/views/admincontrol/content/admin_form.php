<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
$target = $this->action;
if(isset($data)){
	$target .= '/ubah/'.$data->id;
}else{
	$target .= '/addadmin';
}
$level = $this->config->item('admin_level');
?>
				<div class="row">
					<?php echo form_open($target,array("role"=>"form", "class"=>"form-horizontal")) ?>
					<div class="col-sm-offset-2 breadcrumb">
						<h4>Informasi Akun</h4>
					</div>
					
					<div class="form-group">
						<div class="row">
							<?php echo form_label("Nama Akun","name",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php 
							echo form_input(array(
								"name"=>"name", 
								"id"=>"name", 
								"class"=>"form-control", 
								"value"=>html_escape(isset($data) ? $data->name : NULL)
							));
							?>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<?php echo form_label("Email","email",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php
							echo form_input(array(
								"name"=>"email", 
								"id"=>"email", 
								"class"=>"form-control", 
								"value"=>html_escape(isset($data) ? $data->email : NULL)
							));
							?>
							</div>
						</div>
					</div>
				
					<div class="form-group">
						<?php echo form_label("Level","level",array("class"=>"col-sm-2 control-label")) ?>
						<div class="col-sm-10">
						<?php echo form_dropdown('level', $level,isset($data) ? $data->level : "",array('class'=>'form-control')) ?>
						</div>
					</div>
				
<?php
			if(!isset($data)){
?>
<hr>
					<div class="col-sm-offset-2 breadcrumb">
						<h4>Konfigurasi Password </h4>
					</div>
					
					<div class="form-group">
						<div class="row">
							<?php echo form_label("Username","username",array("class"=>"col-sm-2 
							control-label")) ?>
							<div class="col-sm-10">
							<?php 
							echo form_password(array(
								"name"=>"username", 
								"id"=>"username", 
								"class"=>"form-control"
							));
							?>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<?php echo form_label("Password Baru","pass",array("class"=>"col-sm-2 
							control-label")) ?>
							<div class="col-sm-10">
							<?php 
							echo form_password(array(
								"name"=>"pass", 
								"id"=>"pass", 
								"class"=>"form-control"
							));
							?>
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<?php echo form_label("Konfirmasi Password","conf_pass",array("class"=>"col-sm-2 control-label")) ?>
							<div class=" col-sm-10">
							<?php 
							echo form_password(array(
								"name"=>"conf_pass", 
								"id"=>"conf_pass", 
								"class"=>"form-control"
							));
							?>
							</div>
						</div>
					</div>
<?php 			} ?>					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-offset-2 col-sm-3">
								<?php echo form_submit(array("class"=>"btn btn-default"),isset($data) ? "Ubah" : "Simpan") ?>
								<!--button type="reset" class="btn btn-default pull-right">Cancel</button-->
							</div>
						</div>
					</div>

					<?php echo form_close() ?>
					
				</div>
				<script type="text/javascript">
					$(document).ready(function(e){
						if($(".error_message").length != 0){
							$(".error_message")
							.fadeIn("slow")
							.delay(3000)
							.fadeOut("slow")
							.delay(100)
							.queue(function(){
								$(".error_message").remove();
							});
						}
					});
				</script>