<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
if($this->session->flashdata('success')){
	echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
	unset($_SESSION['success']);
}
if($this->session->flashdata('error')){
	echo '<div class="alert alert-warning">'.$this->session->flashdata('error').'</div>';
	unset($_SESSION['error']);
}
echo anchor($this->action.'/addbrand', '<button class="btn btn-default create">Tambah Kategori</button>');
?>
<div class="table-responsive">
	<table class="table table-condensed table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th> Brand </th>
				<th> Tindakan </th>
			</tr>
		</thead>
		<tbody>
		
<?php
$i=0;
foreach($data as $d){
	$i++;
?>
			<tr>
				<td><?php echo $i ?></td>
				<td> <?php echo $d->product_brand ?> </td>
				<td><?php echo anchor($this->action.'/update/'.$d->id,'<button class="btn btn-primary update">Update</button>'); echo anchor($this->action.'/delete/'.$d->id,'<button class="btn btn-danger delete">Delete</button>');?></td>
				<?php
					$attr = array(
						'type'=>'hidden',
						'name'=>'id',
						'value'=>$d->id,
						'id'=>'data'
					); 
					echo form_input($attr);
				?>
			</tr>
<?php
}
?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".delete").click(function(e){
			var conf = confirm('Are you sure want to delete this item ?');
			if(!conf){
				e.preventDefault();
			}
		});
		if($(".alert").length != 0){
			$(".alert").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".alert").remove()});
		}
	});
</script>