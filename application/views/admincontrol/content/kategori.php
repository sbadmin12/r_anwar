<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
if($this->session->flashdata('success')){
	echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
	unset($_SESSION['success']);
}
if($this->session->flashdata('error')){
	echo '<div class="alert alert-warning">'.$this->session->flashdata('error').'</div>';
	unset($_SESSION['error']);
}
echo anchor($this->action.'/addkategori', '<button class="btn btn-default create">Tambah Kategori</button>');
?>
<div class="table-responsive">
	<table class="table table-condensed table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th> Kategori </th>
				<th> Kategori Induk </th>
				<th> Top </th>
				<th> Tindakan </th>
			</tr>
		</thead>
		<tbody>
		
<?php
$i=0;
foreach($data as $d){
	$i++;
?>
			<tr>
				<td><?php echo $i ?></td>
				<td> <?php echo $d->product_category ?> </td>
				<td> 
				<?php
				if($d->parent_id != 0){
					$this->db->from('category');
					$this->db->where('id',$d->parent_id);
					$query = $this->db->get();
					$res = $query->row();
					echo $res->product_category;
				}else{
					echo $d->parent_id;
				}
				
				?> 
				
				</td>
				<td> 
					<?= form_checkbox(array('name'=>'top', 'class'=>'topCategory'), $d->id, $d->top == TRUE ? TRUE : FALSE ) ?>
				</td>
				<td><?php echo anchor($this->action.'/update/'.$d->id,'<button class="btn btn-primary update">Update</button>'); echo anchor($this->action.'/delete/'.$d->id,'<button class="btn btn-danger delete">Delete</button>');?></td>
				<?php
					$attr = array(
						'type'=>'hidden',
						'name'=>'id',
						'value'=>$d->id,
						'id'=>'data'
					); 
					echo form_input($attr);
				?>
			</tr>
<?php
}
?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".delete").click(function(e){
			var conf = confirm('Are you sure want to delete this item ?');
			if(!conf){
				e.preventDefault();
			}
		});
		if($(".alert").length != 0){
			$(".alert").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".alert").remove()});
		}
		$('.topCategory').click(function(){
			var id = null;
			if($(this).prop('checked') == true){
				var conf = confirm('Update kategori ini ke Top Kategori ?');
				if(!conf){
					$(this).prop('checked', false);
				}else{
					id = $(this).val();
					$.post('<?= $this->action ?>/updatetop', {catID:id, select:1}, function(result){
						var e = JSON.parse(result);
						console.log(e);
						
					});
				}
			}else if($(this).prop('checked') == false){
				var conf = confirm('Hapus Top Kategori ?');
				if(!conf){
					$(this).prop('checked', true);
				}else{
					id = $(this).val();
					$.post('<?= $this->action ?>/updatetop', {catID:id, select:0}, function(result){
						var e = JSON.parse(result);
						console.log(e);
					});
				}
			}
		});
	});
	
	/*function updateTop(){
		var topID = document.getElementById("top").value;
		var conf = confirm('Update kategori ini ke Top Kategori ?');
		if(!conf){
			document.getElementById('top').checked = false;
		}else{
			$.post('<?= $this->action ?>/updatetop', {catID:topID}, function(result){
				var e = JSON.parse(result);
				
			});
		}
	}*/
</script>