<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
$target = $this->action;
if(isset($data)){
	$target .= '/update/'.$data->id;
}else{
	$target .= '/addkategori';
}
$options = array();
$options[0] = 'Pilih Kategori Induk';
if(isset($parent)){
	foreach($parent as $c){
		$options[$c->id]=$c->product_category;
	}
}
?>
<?php echo validation_errors("<div class='alert alert-warning error_message' style='display:none'>","</div>") ?>
					<?php echo form_open($target,array("role"=>"form", "class"=>"form-horizontal")) ?>
					
						<div class="form-group">
							<?php echo form_label("Urutan","positions",array("class"=>"col-sm-2 control-label")) ?>
							<div class=" col-sm-10">
							<?php echo form_input(array("name"=>"positions", "id"=>"positions", "class"=>"form-control", "value"=>isset($data) ? $data->positions : NULL)) ?>
							</div>
						</div>
					
						<div class="form-group">
							<?php echo form_label("Nama Kategori","product_category",array("class"=>"col-sm-2 control-label")) ?>
							<div class=" col-sm-10">
							<?php echo form_input(array("name"=>"product_category", "id"=>"product_category", "class"=>"form-control", "value"=>isset($data) ? $data->product_category : NULL)) ?>
							</div>
						</div>
						
						<div class="form-group">
							<?php echo form_label("Kategori Induk","parent_id",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_dropdown('parent_id', $options,isset($data) ? $data->parent_id : 0,array('class'=>'form-control')) ?>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-3">
								<?php echo form_submit(array("class"=>"btn btn-default"),isset($data) ? "Ubah" : "Simpan") ?>
								<!--button type="reset" class="btn btn-default pull-right">Cancel</button-->
								
							</div>
						</div>
						
					<?php echo form_close() ?>