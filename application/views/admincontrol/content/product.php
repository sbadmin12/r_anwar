<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

if($this->session->flashdata('success')){
	echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
	unset($_SESSION['success']);
}
if($this->session->flashdata('error')){
	echo '<div class="alert alert-warning">'.$this->session->flashdata('error').'</div>';
	unset($_SESSION['error']);
}
?>

	<div class="row">
		<div class="col-sm-4">
			<?= anchor($this->action.'/addproduct', '<center><button class="btn btn-md btn-default btn-block create">Tambah Barang</button></center>')  ?>
			
		</div>
		<?= form_open($this->action.'/search', array('role'=>'form', 'id'=>'sorting', 'class'=>'form-inline')) ?>
		
			<div class="col-sm-8">
				<center>
					<input type="text" name="searchVal" class="form-control input-md" placeholder="Search..." />
					<button type="submit" name="submit" class="btn btn-default btn-md"><i class="fa fa-search"></i></button>
				</center>
			</div>
		<?= form_close() ?>
	
	</div>
	<br />
<div class="table-responsive">
	<table class="table table-condensed table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th> Nama Barang </th>
				<th> Kategori </th>
				<th> Harga </th>
				<th> Gambar Barang </th>
				<th> Satuan </th>
				<th> Tindakan </th>
			</tr>
		</thead>
		<tbody>
		
<?php
$i=0;
foreach($data as $d){
	$i++;
?>
			<tr>
				<td><?php echo $i ?></td>
				<td> <?php echo $d->product_name ?> </td>
				<td> <?php echo $d->product_category ?> </td>
				<td> <?php echo $d->product_price ?> </td>
				<td> <img src="<?= image(base_url('images/'.$d->product_image), 'thumb') ?>" alt="<?= $d->product_image ?>" /></td>
				<td> <?php echo $d->product_unit ?> </td>
				<td><?php echo anchor($this->action.'/update/'.$d->id,'<button class="btn btn-primary update">Update</button>'); echo anchor($this->action.'/delete/'.$d->id,'<button class="btn btn-danger delete">Delete</button>');?></td>
			</tr>
<?php
}
?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".delete").click(function(e){
			var conf = confirm('Are you sure want to delete this item ?');
			if(!conf){
				e.preventDefault();
			}
		});
		if($(".alert").length != 0){
			$(".alert").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".alert").remove()});
		}
	});
</script>
	