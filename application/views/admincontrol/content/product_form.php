<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
$target = $this->action;
if(isset($data)){
	$target .= '/update/'.$data->id;
}else{
	$target .= '/addproduct';
}
$options = array();
$kategori = $this->Kategori_model->searchAll(array('parent_id'=>0));
if($kategori){
	foreach($kategori as $c){
		$options[$c->id]=$c->product_category;
		$child = $this->Kategori_model->searchAll(array('parent_id'=>$c->id));
		if($child){
			foreach($child as $c2){
				$options[$c2->id] = '--'.$c2->product_category;
			}
		}
	}
}
$options2 = array();
$brand = $this->Brand_model->searchAll();
if($brand){
	foreach($brand as $c){
		$options2[$c->id] = $c->product_brand;
	}
}
?>
<div class="row">
<?php echo validation_errors("<div class='alert alert-warning error_message' style='display:none'>","</div>") ?>
					
					<?php echo form_open($target,array("role"=>"form", "class"=>"form-horizontal")) ?>
					
						<div class="form-group">
							<?php echo form_label("Nama Barang","product_name",array("class"=>"col-sm-2 control-label")) ?>
							<div class=" col-sm-10">
							<?php echo form_input(array("name"=>"product_name", "id"=>"product_name", "class"=>"form-control", "value"=>isset($data) ? $data->product_name : NULL)) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Kategori","product_category",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_dropdown('product_category', $options,isset($data) ? $data->product_category : '',array('class'=>'form-control')) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Brand","product_brand",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_dropdown('product_brand', $options2, isset($data) ? $data->product_brand : '',array('class'=>'form-control')) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Keterangan","product_description",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_textarea(array("name"=>"product_description", "id"=>"product_description", "class"=>"form-control","value"=>isset($data) ? $data->product_description : NULL, "rows"=>4)) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Harga Barang","product_price",array("class"=>"col-sm-2 control-label")) ?>
							<div class=" col-sm-10">
							<?php echo form_input(array("name"=>"product_price", "id"=>"product_price", "class"=>"form-control", "value"=>isset($data) ? $data->product_price : NULL)) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Satuan","product_unit",array("class"=>"col-sm-2 control-label")) ?>
							<div class=" col-sm-10">
							<?php echo form_input(array("name"=>"product_unit", "id"=>"product_unit", "class"=>"form-control", "value"=>isset($data) ? $data->product_unit : NULL)) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Gambar Barang","product_image",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
    							<input type="file" name="product_image" id="image_btn" />
    							<div id="view">
<?php
	if(isset($data)){
		$original = realpath(FCPATH.DIRECTORY_SEPARATOR.'uploads') . DIRECTORY_SEPARATOR . $data->product_image;
		if(file_exists($original)){
			echo '<img src="'.image(base_url('images/'.$data->product_image),'medium').'" />';
		}else{
			echo "<center>Image Not Found</center>";
		}
		
	}else{
		echo "Image Not Set";
	}
?>
    							</div>
    							<?php echo form_input(array('name'=>'product_image','type'=>'hidden','id'=>'image', 'value'=>isset($data) ? $data->product_image : NULL)) ?>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-3">
								<?php echo form_submit(array("class"=>"btn btn-default"),isset($data) ? "Ubah" : "Tambah") ?>
								<!--button type="reset" class="btn btn-default pull-right">Cancel</button-->
								
							</div>
						</div>
						
					<?php echo form_close() ?>
				</div>
				
				<script type='text/javascript'>
				
				if($(".error_message").length != 0){
					$(".error_message").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".error_message").remove()});
				}
				
				<?php $timestamp = time();?>
			    $(document).ready(function(e){
			    	
			    	$('#image_btn').uploadify({
			    		'formData'     : {
							'timestamp' : '<?php echo $timestamp;?>',
							'token'     : "<?php echo md5('unique_salt' . $timestamp);?>",
							'upload_form':"<?php echo current_url() ?>"
						},
					    'debug'   : false,
					    'swf'   : '<?php echo base_url() ?>assets/images/uploadify.swf',
					    'uploader'  : '<?php echo $this->_admin_url ?>upload/image',
					    'cancelImage' : '<?php echo base_url() ?>assets/images/uploadify-cancel.png',
					    //'queueID'  : 'file-queue',
					    //'buttonClass'  : 'button',
					    'buttonText' : "Upload Files",
					    'multi'   : false,
					    //'auto'   : true,
					    'fileTypeExts' : '*.jpg; *.png; *.gif; *.PNG; *.JPG; *.GIF;',
					    'fileTypeDesc' : 'Image Files',
					    'method'  : 'post',
					    'fileObjName' : 'image',
					    //'queueSizeLimit': 40,
					    //'simUploadLimit': 2,
					    //'sizeLimit'  : 1000,
					    'onUploadSuccess' : function(file, data, response) {
					           var e = JSON.parse(data);
					           if(e.err){
							   	$("#view").empty();
							   	$("#view").append(e.err);
							   }else{
							   $("#view").empty();
					            $("#view").append("<img width='268px' height='249px' src='<?php echo base_url()?>uploads/"+e.file_name+"'>");
					            $("#image").val(e.file_name);
							   }

					    },
					    /*'onUploadComplete' : function(file, data, response) {
					     	//alert('The file ' + file.name + ' finished processing.');
					     	console.log(data);
					     },*/
					     'onQueueFull': function(event, queueSizeLimit) {
					     	alert("Please don't put anymore files in me! You can upload " + queueSizeLimit + " files at once");
					        return false;
					     },
				     });
			    });
			    </script>