<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
if($this->session->flashdata('success')){
	echo '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>';
	unset($_SESSION['success']);
}
if($this->session->flashdata('error')){
	echo '<div class="alert alert-warning">'.$this->session->flashdata('error').'</div>';
	unset($_SESSION['error']);
}
echo anchor($this->action.'/addslider', '<button class="btn btn-default create">Tambah Gambar</button>'); 
?>
<div class="table-responsive">
	<table class="table table-condensed table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th> Gambar </th>
				<th> Aktif </th>
				<th> Tindakan </th>
			</tr>
		</thead>
		<tbody>
<?php
$i=0;
if(isset($data)){
	foreach($data as $d){
		$i++;
?>
			<tr>
				<td><?php echo $i ?></td>
				<td> <?php echo image($d->image,'view') ?> </td>
				<td> <?php echo $d->active == 1 ? "<span class='glyphicon glyphicon-ok'></span>" : "<span class='glyphicon glyphicon-remove'></span>" ; ?> </td>
				<td><?php echo anchor($this->action.'/update/'.$d->id,'<button class="btn btn-primary update">Ubah</button>'); echo anchor($this->action.'/delete/'.$d->id,'<button class="btn btn-danger delete">Hapus</button>');?></td>
			</tr>
<?php 
	} 
}
?>
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(".delete").click(function(e){
			var conf = confirm('Are you sure want to delete this item ?');
			if(!conf){
				e.preventDefault();
			}
		});
		if($(".alert").length != 0){
			$(".alert").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".alert").remove()});
		}
	});
</script>