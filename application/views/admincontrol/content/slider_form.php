<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
$target = $this->action;
if(isset($data)){
	$target .= '/update/'.$data->id;
}else{
	$target .= '/addslider';
}
?>
<div class="row">
<?php echo validation_errors("<div class='alert alert-warning error_message' style='display:none'>","</div>") ?>
					
					<?php echo form_open($target,array("role"=>"form", "class"=>"form-horizontal")) ?>
						<div class="form-group">
							<?php echo form_label("Posisi","positions",array("class"=>"col-sm-2 control-label")) ?>
							<div class=" col-sm-10">
							<?php echo form_input(array("name"=>"positions", "id"=>"positions", "class"=>"form-control", "value"=>isset($data) ? $data->positions : NULL)) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Active","active",array("class"=>"col-sm-2 control-label")) ?>
							<div class=" col-sm-10">
							<?php echo form_checkbox(
								array(
									"name"=>"active", 
									"id"=>"active", 
									"class"=>"form-control", 
									"value"=>isset($data) ? $data->active : 1
								),
								"",
								isset($data) ? ($data->active == 1 ? TRUE : FALSE) : TRUE) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Gambar Slider","image",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
    							<input type="file" name="image" id="image_btn" />
    							<div id="view"><?php echo isset($data) ? '<img src="'.base_url().'uploads/'.$data->image.'">' : NULL; ?></div>
    							<?php echo form_input(array('name'=>'image','type'=>'hidden','id'=>'image', 'value'=>isset($data) ? $data->image : NULL)) ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-3">
								<?php echo form_submit(array("class"=>"btn btn-default"),isset($data) ? "Ubah" : "Tambah") ?>
								<!--button type="reset" class="btn btn-default pull-right">Cancel</button-->
								
							</div>
						</div>
						
					<?php echo form_close() ?>
					
					<script type='text/javascript'>
				$("#active").change(function() {
				    if(this.checked) {
				       $("#active").val('1');
				    }else{
						$("#active").val('0');
					}
					console.log($("#active").val());
				});
				if($(".error_message").length != 0){
					$(".error_message").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".error_message").remove()});
				}
				
				<?php $timestamp = time();?>
			    $(document).ready(function(e){
			    	
			    	$('#image_btn').uploadify({
			    		'formData'     : {
							'timestamp' : '<?php echo $timestamp;?>',
							'token'     : "<?php echo md5('unique_salt' . $timestamp);?>",
							'upload_form':"<?php echo current_url() ?>"
						},
					    'debug'   : false,
					    'swf'   : '<?php echo base_url() ?>assets/images/uploadify.swf',
					    'uploader'  : '<?php echo $this->_admin_url ?>upload/uploadimage',
					    'cancelImage' : '<?php echo base_url() ?>assets/images/uploadify-cancel.png',
					    //'queueID'  : 'file-queue',
					    //'buttonClass'  : 'button',
					    'buttonText' : "Upload Files",
					    'multi'   : false,
					    //'auto'   : true,
					    'fileTypeExts' : '*.jpg; *.png; *.gif; *.PNG; *.JPG; *.GIF;',
					    'fileTypeDesc' : 'Image Files',
					    'method'  : 'post',
					    'fileObjName' : 'image',
					    //'queueSizeLimit': 40,
					    //'simUploadLimit': 2,
					    //'sizeLimit'  : 10240000,
					    'onUploadSuccess' : function(file, data, response) {
					           var e = JSON.parse(data);
					           	$("#view").empty();
					            $("#view").append("<img src='<?php echo base_url()?>uploads/"+e.file_name+"'>");
					            $("#image").val(e.file_name);
					    },
					    /*'onUploadComplete' : function(file, data, response) {
					     	//alert('The file ' + file.name + ' finished processing.');
					     	console.log(data);
					     },*/
					     'onQueueFull': function(event, queueSizeLimit) {
					     	alert("Please don't put anymore files in me! You can upload " + queueSizeLimit + " files at once");
					        return false;
					     },
				     });
			    });
			    </script>