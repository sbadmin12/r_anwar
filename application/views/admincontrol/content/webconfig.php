<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

?>

				<div class="row">
					<?php echo form_open($this->action,array("role"=>"form", "class"=>"form-horizontal")) ?>
					<?php echo validation_errors("<div class='alert alert-warning'>","</div>") ?>
					<?php
						/*echo !empty($this->session->flashdata('config_msg')) ? "<div class='alert alert-success'>".$this->session->flashdata('config_msg')."</div>" : "";
						echo !empty($this->session->flashdata('config_err')) ? "<div class='alert alert-danger'>".$this->session->flashdata('config_err')."</div>" : "";*/
					?>
						<div class="form-group">
							<?php echo form_label("Website Name","web_name",array("class"=>"col-sm-2 
							control-label")) ?>
							<div class=" col-sm-10">
							<?php echo form_input(array("name"=>"web_name", "id"=>"web_name", "class"=>"form-control", "value"=>$res['web_name'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Website Title","web_title",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_input(array("name"=>"web_title", "id"=>"web_title", "class"=>"form-control", "value"=>$res['web_title'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Website Description","web_description",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_textarea(array("name"=>"web_description", "id"=>"web_description", "class"=>"form-control", "rows"=>4, "value"=>$res['web_description'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Website Keywords","web_keywords",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_textarea(array("name"=>"web_keywords", "id"=>"web_keywords", "class"=>"form-control", "rows"=>4, "value"=>$res['web_keywords'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Website Email","web_email",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_input(array("name"=>"web_email", "id"=>"web_email", "class"=>"form-control", "value"=>$res['web_email'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Address","address",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_textarea(array("name"=>"address", "id"=>"address", "class"=>"form-control", "rows"=>4, "value"=>$res['address'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Phone","phone",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_input(array("name"=>"phone", "id"=>"phone", "class"=>"form-control", "value"=>$res['phone'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Fax","fax",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_input(array("name"=>"fax", "id"=>"fax", "class"=>"form-control", "value"=>$res['fax'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Facebook","facebook",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_input(array("name"=>"facebook", "id"=>"facebook", "class"=>"form-control", "value"=>$res['facebook'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Twitter","twitter",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
							<?php echo form_input(array("name"=>"twitter", "id"=>"twitter", "class"=>"form-control", "value"=>$res['twitter'])) ?>
							</div>
						</div>
						<div class="form-group">
							<?php echo form_label("Logo","web_logo",array("class"=>"col-sm-2 control-label")) ?>
							<div class="col-sm-10">
								<input id="web_logo" name="web_logo" type="file"  multiple="true"> ukuran yang disarankan 150 x 120
								<div id="view">
									<?php echo isset($res['web_logo']) ? '<img src="'.base_url().'uploads/'.$res['web_logo'].'">' : NULL; ?>
								</div>
								<?php echo form_input(array('name'=>'web_logo','type'=>'hidden','id'=>'image', 'value'=>isset($res['web_logo']) ? $res['web_logo'] : NULL)) ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-3">
								<?php echo form_submit(array("class"=>"btn btn-default"),"Save Config") ?>
								<!--button type="reset" class="btn btn-default pull-right">Cancel</button-->
							</div>
						</div>
						
					<?php echo form_close() ?>
				</div>
				<script type="text/javascript">
				if($(".alert").length != 0){
					$(".alert").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".alert").remove()});
				}
					<?php $timestamp=time();?>
					$('#web_logo').uploadify({
						'formData':{
							'timestamp':'<?php echo $timestamp;?>',
							'token':"<?php echo md5('unique_salt' . $timestamp);?>",
							'upload_form':"<?php echo current_url() ?>"
						},
						'debug':false,
						'swf':'<?php echo base_url() ?>assets/images/uploadify.swf',
						'uploader':'<?php echo $this->_admin_url ?>upload/uploadimage',
						'cancelImage':'<?php echo base_url() ?>assets/images/uploadify-cancel.png',
						'buttonText':"Upload Files",
						'multi':false,
						scriptAccess: 'always',
						'fileTypeExts':'*.jpg; *.png; *.gif; *.PNG; *.JPG; *.GIF;',
						'fileTypeDesc':'Image Files',
						'method':'post',
						'fileObjName':'image',
						'onUploadSuccess':function(file,data,response){
							var e=JSON.parse(data);
							$("#view").empty();
							$("#view").append("<img src='<?php echo base_url()?>uploads/"+e.file_name+"'>");
							$("#image").val(e.file_name);
						},
						'onQueueFull':function(event,queueSizeLimit){
							alert("Please don't put anymore files in me! You can upload "+queueSizeLimit+" files at once");
							return false;
						},
						'onError'     : function (event,ID,fileObj,errorObj) {
              				console.log(errorObj.type + ' Error: ' + errorObj.info);
           				 },
					});
					$(document).ready(function(e){
						if($(".error_message").length != 0){
							$(".error_message").
								fadeIn("slow").
								delay(3000).
								fadeOut("slow").
								delay(100).
								queue(function(){
									$(".error_message").remove();
								});
						}
					});
				</script>
				