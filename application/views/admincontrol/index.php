<?php
    defined('BASEPATH') OR exit ('no direct script access allowed');
    echo doctype('html5');
    echo '<html lang="en">';
    $this->load->view('admincontrol/template/head');
?>

<body>
<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> <!-- Navbar Fixed -->
        <?php
        $this->load->view('admincontrol/template/navbar');
        $this->load->view('admincontrol/template/sidebar');
        ?>
    </nav> <!-- End of Navbar Fixed -->

    <div id="page-wrapper"> <!-- Page Wrapper -->
        <div class="container-fluid"> <!-- Container -->

            <div class="row"> <!-- Page Heading -->
                <div class="col-lg-12">

                    <?php $this->load->view('admincontrol/template/header') ?>

                </div>
            </div> <!-- End of Page Heading -->
            <?php
            $this->load->view(isset($content) ? "admincontrol/content/".$content : "admincontrol/content/home");
            $this->load->view('admincontrol/template/footer');
            ?>
        </div> <!-- End of Container -->
    </div><!-- End of Page Wrapper -->
</div> <!-- End of Wrapper -->


</body>
</html>