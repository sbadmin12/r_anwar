<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
?>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Admin Site Bola Dunia </title>

    <?php
    	$source = array(
    		base_url().'assets/css/bootstrap.css',
    		base_url().'assets/css/uploadify.css',
    		base_url().'assets/css/sb-admin.css',
    		base_url().'assets/css/plugins/morris.css',
    		base_url().'assets/css/jquery-ui.css',
    		base_url().'assets/font-awesome/css/font-awesome.min.css'
    	);
    	for($i=0; $i<count($source); $i++){
			echo link_tag($source[$i],'stylesheet','text/css','','',TRUE);
		}
        
       
    ?>
    <!-- Javascript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.uploadify.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
