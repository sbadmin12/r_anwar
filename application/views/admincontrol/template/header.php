<?php
defined('BASEPATH') OR exit("no direct script access allowed");
?>

<h1 class="page-header">
    <?php echo $info['header'] ?>
</h1>
<ol class="breadcrumb">
    <?php
    for($i=0; $i<count($info['map']); $i++){
		echo '<li>'.$info['map'][$i].'</li>';
    }
    ?>
    
</ol>