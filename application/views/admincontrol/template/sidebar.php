<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
$session = $this->session->userdata('logged_user_level');

?>
<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        
        <li>
            <a href="javascript:;" data-toggle="collapse" data-target="#barang"><i class="fa fa-fw fa-book"></i> Barang <div class="pull-right"><i class="fa fa-fw fa-caret-down"></i></div></a>
            <ul id="barang" class="collapse">
               
                 <li>
                    <a href="<?php echo $this->_admin_url ?>kategori">Kategori</a>
                </li>
                
                <li>
                    <a href="<?php echo $this->_admin_url ?>brand">Brands</a>
                </li>
               
				<li>
                    <a href="<?php echo $this->_admin_url ?>product"> Barang </a>
                </li>
            </ul>
        </li>
        
        <li>
            <a href="javascript:;" data-toggle="collapse" data-target="#konfigurasi"><i class="fa fa-fw fa-wrench"></i> Konfigurasi <div class="pull-right"><i class="fa fa-fw fa-caret-down"></i></div></a>
            <ul id="konfigurasi" class="collapse">
               <?php if($this->_auth === TRUE){
				?>
                 <li>
                    <a href="<?php echo $this->_admin_url ?>admin">Admin</a>
                </li>
                <?php	
				}
				?>
				<li>
                    <a href="<?php echo $this->_admin_url ?>webconfig"> Website Data</a>
                </li>
                <li>
                    <a href="<?php echo $this->_admin_url ?>slider"> Slider</a>
                </li>
            </ul>
        </li>
    </ul>
</div>