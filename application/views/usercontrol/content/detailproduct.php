<?php defined('BASEPATH') OR exit ('no direct script access allowed'); ?>
<?php if(isset($data) && $data != ''){ ?>
<?php
	$brand_name = NULL;
	$brand = $this->Brand_model->searchAll(array('id'=>$data->product_brand));
	if($brand){
		foreach($brand as $res1){
			$brand_name = $res1->product_brand;
		}
	}
?>

					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<div class="image-box-large"><img src="<?= image(base_url('images/'.$data->product_image),'large') ?>" class="img img-responsive" /></div>
							</div>
						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								
								<h2><?= $data->product_name ?></h2>
								
								<span>
									<span><?= $data->product_price ?></span>
								</span>
								<table>
									<tr>
										<td> Merk </td>
										<td> : </td>
										<td> <?= $brand_name ?> </td>
									</tr>
									<tr>
										<td> Satuan </td>
										<td> : </td>
										<td> <?= $data->product_unit ?></td>
									</tr>
									<tr>
										<td colspan="3"><p style="margin: 20px 0px"><?= nl2br($data->product_description) ?></p></td>
									</tr>
								</table>
								<p id="share-btn">
									<b>Bagikan : </b>&nbsp; &nbsp;
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?= current_url() ?>"><i class="fa fa-lg fa-fw fa-facebook"></i></a>&nbsp; &nbsp;
									<a href="https://twitter.com/home?status=<?= current_url() ?>"><i class="fa fa-lg fa-fw fa-twitter"></i></a>&nbsp; &nbsp;
								</p>
								<!--a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a-->
							</div><!--/product-information-->
						</div>
					</div>
<?php
}
$product2 = $this->Product_model->searchAll(array(),NULL, array('rand()',NULL), 21);
if($product2){
?>

					<div class="recommended_items">
						<h2 class="title text-center">Rekomendasi</h2>
						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
<?php
	$i=1;
	$s = 0;
	foreach($product2 as $res2){
		if($i == 1){
?>

								<div class="item<?= $s == 0 ? ' active' : NULL ?>">
<?php
		}
?>

									<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<div class="image-box-small"><img src="<?= image(base_url('images/'.$res2->product_image),'small') ?>" /></div>
													
													<h2> <?= $res2->product_price ?></h2>
													<p> <?= $res2->product_name ?></p>
													<a href="<?= base_url('product/'.$res2->id.'/'.slug($res2->product_name)) ?>" class="btn btn-default add-to-cart"><!--i class="fa fa-shopping-cart"></i--><i class="fa fa-asterisk"></i>Details</a>
												</div>
											</div>
										</div>
									</div>
<?php
		if($i == 3 || $s == 5){
?>

								</div>
<?php
		}
		if($i == 3){
			$i = 1;
		}else{
			$i++;
		}
		$s++;
	}
?>
							</div>
							<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							</a>
							<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							</a>
						</div>
					</div>
<?php
}
?>