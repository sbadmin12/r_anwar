<?php defined('BASEPATH') OR exit ('no direct script access allowed'); ?>
								
							<div class="features_items">
								<h2 class="title text-center"> Produk </h2>
<?php
$product = $this->Product_model->searchAll(array(),NULL, array('rand()',NULL), 6);
if($product){
	foreach($product as $res){
?>

								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<div class="image-box-medium"><img src="<?= image(base_url('images/'.$res->product_image),'medium') ?>" /></div>
												
												<h2><?= $res->product_price ?></h2>
												<p><?= $res->product_name ?></p>
												<a href="<?= base_url('product/'.$res->id.'/'.slug($res->product_name)) ?>" class="btn btn-default add-to-cart"><i class="fa fa-asterisk"></i>Details</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2><?= $res->product_price ?></h2>
													<p><?= $res->product_name ?></p>
													<a href="<?= base_url('product/'.$res->id.'/'.slug($res->product_name)) ?>" class="btn btn-default add-to-cart"><i class="fa fa-asterisk"></i>Details</a>
												</div>
											</div>
										</div>
									</div>
								</div>
<?php
	}
}
?>

							</div>
<?php

$cat = $this->Kategori_model->searchAll(array('top'=>1));
if($cat){
	$c = 0;
?>

							<div class="category-tab">
								<div class="col-sm-12">
									<ul class="nav nav-tabs">
<?php
	foreach($cat as $res){
?>

										<li class="<?= $c === 0 ? 'active' : NULL ?>"><a href="#<?= slug($res->product_category) ?>" data-toggle="tab"><?= $res->product_category ?></a></li>
<?php
		$c++;
	}
?>

										<li class="pull-right"><a href="#">Top Kategori</a></li>									
									</ul>
								</div>
								<div class="tab-content">
<?php
$c = 0;
	foreach($cat as $res1){
		
?>

									<div class="tab-pane fade<?= $c === 0 ? ' active in': NULL ?>" id="<?= slug($res1->product_category) ?>">
<?php
		if($res1->parent_id == 0){
			$cat2 = $this->Kategori_model->searchAll(array('parent_id'=>$res1->id), NULL, array('rand()',NULL), 6);
			if($cat2){
				foreach($cat2 as $res2){
					$product1 = $this->Product_model->searchAll(array('product_category'=>$res2->id), NULL, array('rand()',NULL), 6);
					if($product1){
						foreach ($product1 as $res3){
?>

										<div class="col-sm-3">
											<div class="product-image-wrapper">
												<div class="single-products">
													<div class="productinfo text-center">
														<div class="image-box-small"><img src="<?= image(base_url('images/'.$res3->product_image),'small') ?>" alt="" /></div>
														
														<h2><?= $res3->product_price ?></h2>
														<p><?= $res3->product_name ?></p>
														<a href="<?= base_url('product/'.$res3->id.'/'.slug($res3->product_name)) ?>" class="btn btn-default add-to-cart"><i class="fa fa-asterisk"></i>Details</a>
													</div>
													
												</div>
											</div>
										</div>
<?php
						}
					}
				}
				
			}
		}else{
			$product2 = $this->Product_model->searchAll(array('product_category'=>$res1->id));
			if($product2){
				foreach ($product2 as $res4){
?>

										<div class="col-sm-3">
											<div class="product-image-wrapper">
												<div class="single-products">
													<div class="productinfo text-center">
														<div class="image-box-small"><img src="<?= image(base_url('images/'.$res4->product_image),'small') ?>" alt="" /></div>
														
														<h2><?= $res4->product_price ?></h2>
														<p><?= $res4->product_name ?></p>
														<a href="<?= base_url('product/'.$res4->id.'/'.slug($res4->product_name)) ?>" class="btn btn-default add-to-cart"><i class="fa fa-asterisk"></i>Details</a>
													</div>
													
												</div>
											</div>
										</div>
<?php
				}
			}
		}
?>				
									</div>
<?php
		$c++;
	}
?>

								</div>
							</div>
<?php
}
$product2 = $this->Product_model->searchAll(array(),NULL, array('rand()',NULL), 21);
if($product2){
?>

							<div class="recommended_items">
								<h2 class="title text-center">Rekomendasi</h2>
								<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
									<div class="carousel-inner">
<?php
	$i=1;
	$s = 0;
	foreach($product2 as $res2){
		if($i == 1){
?>

										<div class="item<?= $s == 0 ? ' active' : NULL ?>">
<?php
		}
?>

											<div class="col-sm-4">
												<div class="product-image-wrapper">
													<div class="single-products">
														<div class="productinfo text-center">
															<div class="image-box-small"><img src="<?= image(base_url('images/'.$res2->product_image),'small') ?>" /></div>
															
															<h2> <?= $res2->product_price ?></h2>
															<p> <?= $res2->product_name ?></p>
															<a href="<?= base_url('product/'.$res2->id.'/'.slug($res2->product_name)) ?>" class="btn btn-default add-to-cart"><!--i class="fa fa-shopping-cart"></i--><i class="fa fa-asterisk"></i>Details</a>
														</div>
													</div>
												</div>
											</div>
<?php
		if($i == 3 || $s == 5){
?>

										</div>
<?php
		}
		if($i == 3){
			$i = 1;
		}else{
			$i++;
		}
		$s++;
	}
?>
									</div>
									<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
										<i class="fa fa-angle-left"></i>
									</a>
									<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
										<i class="fa fa-angle-right"></i>
									</a>
								</div>
							</div>
<?php
}
?>
