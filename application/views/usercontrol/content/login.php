<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
echo doctype('html5');
?>


<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>
        <?php
        $query =  $this->db->get_where('web_config', array('name' => 'web_title'))->row();
        echo $query->val;
        ?>
    </title>
    <?php
    echo link_tag('assets/css/login.css');
    echo link_tag('assets/css/bootstrap.css');
    ?>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>
</head>
<body>
<div class="container">
    <section id="content">
    
        <?php echo form_open(base_url().'sbadmin',array('method'=>'post')) ?>

        <h1>Login Form</h1>
        <div>
            <?php echo form_input(array('name'=>'username','placeholder'=>'Username','id'=>'username')) ?>
        </div>
        <div>
            <?php echo form_password(array('name'=>'pass','placeholder'=>'Password','id'=>'pass')) ?>
        </div>
        <div>
            <?php echo form_submit('','Login')?>
            <!--a href="#">Lost your password?</a-->
        </div>
        <?php echo form_close() ?><!-- form -->
        <div class="button">
            <!--a href="#"></a-->
            <?php  $query =  $this->db->get_where('web_config', array('name' => 'web_name'))->row(); echo $query->val; ?>
        </div><!-- button -->
    </section><!-- content -->
    <section id="error">
        <?php echo validation_errors("<div class='alert alert-warning error_message' style='display:none'>","</div>") ?>
        <?php
        /*echo isset($this->session->flashdata('err')) ? "<div class='alert alert-danger error_message'>".$this->session->flashdata('err')."</div>": "";*/
        ?>
        
    </section>
</div><!-- container -->
<script type="text/javascript">
    if($(".error_message").length != 0){
        $(".error_message").fadeIn("slow").delay(3000).fadeOut("slow").delay(100).queue(function(){$(".error_message").remove()});
    }
</script>
</body>
</html>