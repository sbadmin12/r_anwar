<?php
defined('BASEPATH') OR exit ('no direct script access allowed');
?>

<div id="contact-page" class="container">
	<div class="bg">
		<div class="row">
			<div class="col-md-8">
				<h2 class="title text-center">Peta</h2>
				<div id="gmap" class="contact-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.966607719475!2d106.82195461497211!3d-6.135189061858515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1e020472486b%3A0x757b82c78047e410!2sPD.+Bola+Dunia+Stationery!5e0!3m2!1sen!2sid!4v1449728252167" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-sm-4">
			<div class="contact-info">
	    				<h2 class="title text-center">Info Lokasi</h2>
	    				<address>
	    					<p><?php  $query =  $this->db->get_where('web_config', array('name' => 'web_name'))->row(); echo nl2br($query->val); ?></p>
	    					<br />
							<p><?php  $query =  $this->db->get_where('web_config', array('name' => 'address'))->row(); echo nl2br($query->val); ?></p>
							<br />
							<p>Telepon : <?php  $query =  $this->db->get_where('web_config', array('name' => 'phone'))->row(); echo nl2br($query->val); ?></p>
							<p>Fax : <?php  $query =  $this->db->get_where('web_config', array('name' => 'fax'))->row(); echo nl2br($query->val); ?></p>
							<p>Email : <?php  $query =  $this->db->get_where('web_config', array('name' => 'web_email'))->row(); echo nl2br($query->val); ?></p>
	    				</address>
	    				<div class="social-networks">
	    					<h2 class="title text-center">Sosial Media</h2>
							<ul>
								<li>
									<a href="<?php  $query =  $this->db->get_where('web_config', array('name' => 'facebook'))->row(); echo nl2br($query->val); ?>"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="<?php  $query =  $this->db->get_where('web_config', array('name' => 'twitter'))->row(); echo nl2br($query->val); ?>"><i class="fa fa-twitter"></i></a>
								</li>
							</ul>
	    				</div>
	    			</div>
			</div>
		</div>
	</div>
</div>