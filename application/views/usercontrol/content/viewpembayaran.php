<style type="text/css">
#method{
	   display: block;
    position: relative;
    width: 100%;
    height: 500px;
}
</style>
<div id="method">
<h2 class="title text-center">Pemesanan</h2>

			<a href="#desc1"> <div class="title1" id="title1" >Melalui Telepon</div></a>
			<a href="#desc2"> <div class="title1" id="title2" >Melalui Email</div></a>
			<a href="#desc3"> <div class="title1" id="title3" >Daftar Nomor Rekening</div></a>
			
			<div class="desc" id="desc1">
			<i class="fa fa-phone-square fa-lg"></i>&nbsp; Telepon ke nomor yang tertera pada bagian bawah website<br /><br />
			<i class="fa fa-exchange fa-lg"></i>&nbsp; Lakukan Pemesanan secara langsung dan tentukan cara pengiriman<br /><br />
			<i class="fa fa-credit-card fa-lg"></i>&nbsp; Transfer Ke rekening yang tercantum<br /><br />
			<i class="fa fa-check-square fa-lg"></i>&nbsp; Lakukan konfirmasi pembayaran dengan cara menghubungi kembali atau dengan sms<br /><br />
			<i class="fa fa-paper-plane fa-lg"></i>&nbsp; Barang yang dipesan akan segera dikrim setelah mendapat konfirmasi<br /><br />
			<i class="fa fa-search fa-lg"></i>&nbsp; Mengetahui  status pengiriman anda akan diberikan no resi pengiriman barang dalam waktu yang telah ditetapkan<br /><br />
			
			</div>
			<div class="desc" id="desc2">
			<i class="fa fa-envelope fa-lg"></i>&nbsp; Klik link E-Mail di bagian bawah website atau bisa mengirimkannya secara manual ke alamat yang tertera<br /><br />
			<i class="fa fa-tasks fa-lg"></i>&nbsp; Cantumkan nama barang, jumlah, Alamat Pengiriman, Nomor telepon yang dapat dihubungi<br /><br />
			<i class="fa fa-share fa-lg"></i>&nbsp; Anda akan dikonfirmasi kembali melalui E-Mail atau dihubungi ke nomor yang sudah dicantumkan didalam E-Mail<br /><br />
			<i class="fa fa-credit-card fa-lg"></i>&nbsp; Jika benar anda akan diminta untuk melakukan pembayaran<br /><br />
			<i class="fa fa-check-square fa-lg"></i>&nbsp; Lakukan konfirmasi pembayaran dengan cara menghubungi kembali atau dengan sms<br /><br />
			<i class="fa fa-paper-plane fa-lg"></i>&nbsp; Barang yang dipesan akan segera dikrim setelah mendapat konfirmasi<br /><br />
			<i class="fa fa-search fa-lg"></i>&nbsp; Mengetahui  status pengiriman anda akan diberikan no resi pengiriman barang dalam waktu yang telah ditetapkan<br /><br />
			</div>
			<div class="desc" id="desc3">
			<table class="rek">
				<tr>
					<td rowspan=3>
					<img src= "<?php echo base_url() ?>assets/images/bca.jpg" width="160px" height="60px" class="img img-responsive" />
					</td>
				</tr>
				<tr>
					<td> No Rekening</td>
					<td> : </td>
					<td> 335 105 430 0 </td>
				</tr>
				<tr>
					<td> Atas Nama</td>
					<td> : </td>
					<td> Anwar Kuswadi</td>
				</tr>
			</table>
			
			<table class="rek">
				<tr>
					<td rowspan=3>
					<img src= "<?php echo base_url() ?>assets/images/mandiri.png" width="160px" height="60px" class="img img-responsive" />
					</td>
				</tr>
				<tr>
					<td> No Rekening</td>
					<td> : </td>
					<td> 115 000 575 688 9 </td>
				</tr>
				<tr>
					<td> Atas Nama</td>
					<td> : </td>
					<td> Anwar Kuswadi</td>
				</tr>
			</table>
			<style type="text/css">
				.rek{
					margin-bottom: 50px !important;
				}
				.rek tr td{
					padding: 5px 10px;
				}
			</style>
			</div>
</div>