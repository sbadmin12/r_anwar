<?php defined('BASEPATH') OR exit ('no direct script access allowed'); ?>

							<div class="features_items">
								<h2 class="title text-center"> Produk </h2>
<?php
if(isset($product) && !empty($product)){
	foreach($product as $res){
?>

								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<div class="image-box-medium"><img src="<?= image(base_url('images/'.$res->product_image),'medium') ?>" /></div>
												<h2><?= $res->product_price ?></h2>
												<p><?= $res->product_name ?></p>
												<a href="<?= base_url('product/'.$res->id.'/'.slug($res->product_name)) ?>" class="btn btn-default add-to-cart"><i class="fa fa-asterisk"></i>Details</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2><?= $res->product_price ?></h2>
													<p><?= $res->product_name ?></p>
													<a href="<?= base_url('product/'.$res->id.'/'.slug($res->product_name)) ?>" class="btn btn-default add-to-cart"><i class="fa fa-asterisk"></i>Details</a>
												</div>
											</div>
										</div>
									</div>
								</div>
<?php
	}
}else{
?>

								<div class="text-center">
<?php
	if(isset($message) && !empty($message)){
		echo $message;
	}else{
		echo "No Product Found Here";
	}
?>
								</div>
<?php
}
?>

							</div>
