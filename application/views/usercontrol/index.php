<?php
defined('BASEPATH') OR exit ('no direct_script access allowed');

if($content == "login"){
    include "content/".$content.".php";
}else{
?>
<!DOCTYPE html>
<html lang="en">
	<?php include "template/head.php" ?>
	
	<body>
		<?php include "template/header.php"; ?>
		<?php include "template/slider.php"; ?>
		<?php if(uri_string() != 'contact'){ ?>
		
		<section>
			<div class="container">
				<div class="row">
					<div class="col-sm-3">
						<?php include "template/sidebar.php"; ?>
							
					</div>
					<div class="col-sm-9 padding-right">
						<?php isset($content) ? include "content/".$content.".php" : include "content/homepage.php";?>
					
					</div>
				</div>
			</div>
		</section>
		<?php }else{ ?>
		
		<?php isset($content) ? include "content/".$content.".php" : include "content/homepage.php";?>
		
		<?php } ?>
		
		<?php include "template/footer.php"; ?>
		
		<!-- Javascript -->
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.scrollUp.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/price-range.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/main.js"></script>
	</body>
</html>
    <?php
}
?>

 