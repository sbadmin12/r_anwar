<?php defined('BASEPATH') OR exit ('no direct script access allowed') ?>

		<footer id="footer">
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-sm-3">
							<div class="companyinfo">
								<div class="logo"><?php  $query =  $this->db->get_where('web_config', array('name' => 'web_name'))->row(); echo $query->val; ?></div>
								<p><?php  $query =  $this->db->get_where('web_config', array('name' => 'web_description'))->row(); echo $query->val; ?></p>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="single-widget">
								<h2> Kategori </h2>
<?php
$kategori = $this->Kategori_model->searchAll(array('parent_id'=>0));
if($kategori){
?>

								<ul class="nav nav-pills nav-stacked">
<?php
	foreach($kategori as $res){
?>

									<li><a href="#"><?= $res->product_category ?></a></li>
<?php
	}
?>
								</ul>
<?php
}
?>
									
							</div>
						</div>
						<div class="col-sm-3">
							<div class="single-widget">
								<h2> Support </h2>
								<img src="<?= base_url('assets/images/logo-BCA-Mandiri-Pembayaran.jpg') ?>" width="100%" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<p class="pull-left">Copyright &copy; 2015</p>
						<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
					</div>
				</div>
			</div>
		</footer>