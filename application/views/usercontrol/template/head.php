<?php
defined('BASEPATH') OR exit ('no direct script access allowed');

?>
	<head>

	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="keywords" content="<?php  $query =  $this->db->get_where('web_config', array('name' => 'web_keywords'))->row(); echo $query->val; ?>">
	    <meta name="description" content="<?php  $query =  $this->db->get_where('web_config', array('name' => 'web_description'))->row(); echo $query->val; ?>">
	    <meta name="author" content="">

	    <title> <?php  $query =  $this->db->get_where('web_config', array('name' => 'web_title'))->row(); echo $query->val; ?> </title>

		<!-- Icon Website -->
		<link rel="shortcut icon" href="<?php $query =  $this->db->get_where('web_config', array('name' => 'web_logo'))->row(); /*echo get_url($query->val,TRUE,'icon')*/ ?>"/>
		 <?php
    	$source = array(
    		base_url().'assets/css/bootstrap.css',
			base_url().'assets/font-awesome/css/font-awesome.min.css',
			base_url().'assets/css/prettyPhoto.css',
			base_url().'assets/css/animate.css',
			base_url().'assets/css/main.css',
			base_url().'assets/css/responsive.css',
			//base_url().'assets/css/style.css',
    	);
    	for($i=0; $i<count($source); $i++){
?>

		<?= link_tag($source[$i],'stylesheet','text/css','','',TRUE); ?>
<?php
		}
    ?>
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

	</head>