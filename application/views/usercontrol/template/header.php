		
		<header id="header">
			<div class="header_top">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="contactinfo">
								<ul class="nav nav-pills">
									<?php  $query =  $this->db->get_where('web_config', array('name' => 'phone'))->row(); echo '<li><a href="tel:'.$query->val.'"><i class="fa fa-lg fa-phone"></i>&nbsp; '.$query->val.'</a></li>'; ?>
									
									<?php  $query =  $this->db->get_where('web_config', array('name' => 'web_email'))->row(); echo '<li><a href="mailto:'.$query->val.'"><i class="fa fa-lg fa-envelope"></i>&nbsp; '.$query->val.'</a></li>'; ?>
								</ul>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="social-icons pull-right">
								<ul class="nav navbar-nav">
									<?php  $query =  $this->db->get_where('web_config', array('name' => 'facebook'))->row(); echo'<li><a href="'.$query->val.'"><i class="fa fa-lg fa-facebook"></i></a></li>'; ?>
									
									<?php  $query =  $this->db->get_where('web_config', array('name' => 'twitter'))->row(); echo '<li><a href="'.$query->val.'"><i class="fa fa-lg fa-twitter"></i></a></li>'; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="header-middle">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="logo pull-left">
								<?php  $query =  $this->db->get_where('web_config', array('name' => 'web_name'))->row(); echo $query->val; ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="header-bottom">
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<?php include 'navbar.php' ?>
							
						</div>
						<div class="col-sm-3">
							<form class="search_box pull-right" method="post" action="<?= base_url('search') ?>">
								<input name="query" type="text" placeholder="search..." />
							</form>
						</div>
					</div>
				</div>
			</div>
		</header>