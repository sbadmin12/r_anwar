<?php defined('BASEPATH') OR exit ('no direct script access allowed'); ?>

							<div class="left-sidebar">
								<h2> Kategori </h2>
								<div class="panel-group category-products" id="accordian">
<?php
$kategori = $this->Kategori_model->searchAll(array('parent_id'=>0));
if($kategori){
	foreach($kategori as $res){
?>

									<div class="panel panel-default">
<?php
		$turunan = $this->Kategori_model->searchAll(array('parent_id'=>$res->id));
		if($turunan){
?>

										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordian" href="#<?= $res->id ?>">
													<span class="badge pull-right">
														<i class="fa fa-arrows"></i>
													</span>
													<?= $res->product_category ?>
													
												</a>
											</h4>
										</div>
										<div id="<?= $res->id ?>" class="panel-collapse collapse">
											<div class="panel-body">
												<ul>
<?php
			foreach($turunan as $res2){
?>
				
													<li><a href="<?php echo base_url('category/'.$res2->id.'/'.slug($res2->product_category)) ?>"><?= $res2->product_category ?></a></li>
<?php
			}
?>

												</ul>
											</div>
										</div>
<?php
		}else{
?>

										<div class="panel-heading">
											<h4 class="panel-title"><a href="<?php echo base_url('category/'.$res->id.'/'.slug($res->product_category)) ?>"><?= $res->product_category ?></a></h4>
										</div>
<?php
		}
?>

									</div>
<?php
	}
}
?>
								</div>
								<div class="brands_products">
									<h2> Brands </h2>
									<div class="brands-name">
<?php
$brand = $this->Brand_model->searchAll();
if($brand){
?>

										<ul class="nav nav-pills nav-stacked">
<?php
	foreach($brand as $res){
		
	$count = $this->Product_model->searchAll(array('product_brand'=>$res->id),NULL,array(),NULL,TRUE);
?>

											<li><a href="<?php echo base_url('brand/'.$res->id.'/'.slug($res->product_brand)) ?>"><span class="pull-right">(<?= $count ?>)</span> <?= $res->product_brand ?></a></li>
<?php
	}
?>
										</ul>
<?php
}
?>
									</div>
								</div>
							</div>