-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 29 Feb 2016 pada 06.24
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `u339403921_demo`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admincontrol`
--

CREATE TABLE IF NOT EXISTS `admincontrol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `level` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `admincontrol`
--

INSERT INTO `admincontrol` (`id`, `name`, `email`, `username`, `pass`, `level`) VALUES
(7, 'Jesica Martha', 'jesicamartha1@gmail.com', 'jesica', '94e44f9854bfe610d50edbc2b3b1f959', 3),
(8, 'Andhy Taslin', 'andhy.taslin@itconcept.sg', 'andhy', 'c93bfbb20a724c2908570aa8b86f8517', 6),
(9, 'Anwar', 'Anwar@gmail.com', 'anwar', '3e03487ccf6e2df31b1785a6cc2e8236', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_brand` varchar(100) DEFAULT NULL,
  `positions` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data untuk tabel `brand`
--

INSERT INTO `brand` (`id`, `product_brand`, `positions`) VALUES
(4, 'Royal', 0),
(5, 'Namas', 1),
(6, 'Standart', 2),
(7, 'Faster', 3),
(8, 'Joyko', 4),
(9, 'Sinar Dunia', 5),
(10, 'Bantex', 6),
(11, 'Kenko', 7),
(12, 'Bintang Obor', 8),
(13, 'SS', 9),
(14, 'Natural', 10),
(15, 'Epson', 11),
(16, 'Kingstone', 12),
(17, 'HP', 13),
(18, 'Canon', 14),
(19, 'Sandisk', 15),
(20, 'Ria', 16),
(21, 'Benex', 17),
(22, 'Nachi', 18),
(23, 'Butterfly', 19),
(24, 'Joy-Art', 20),
(25, 'max', 21),
(26, 'zrm', 22),
(27, 'Sakato', 23);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category` varchar(100) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `top` tinyint(1) NOT NULL,
  `positions` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `product_category`, `parent_id`, `top`, `positions`) VALUES
(72, 'Double Tape', 70, 0, '9'),
(71, 'Lakban', 70, 0, '8'),
(67, 'Kwitansi', 66, 0, '4'),
(66, 'Buku Tulis', 0, 1, '3'),
(65, 'Continuous Form', 58, 0, '2'),
(64, 'HVS', 58, 0, '1'),
(58, 'Kertas', 0, 1, '0'),
(70, 'Tape dan Perekat', 0, 0, '7'),
(69, 'Buku Tulis Sekolah', 66, 0, '6'),
(68, 'Hard Cover', 66, 0, '5'),
(73, 'Lem', 70, 0, '10'),
(74, 'Selotip', 70, 0, '11'),
(75, 'Alat Tulis', 0, 0, '12'),
(76, 'Ballpoint', 75, 0, '13'),
(77, 'Pensil, Penghapus, & Penggaris', 75, 0, '14'),
(78, 'Spidol', 75, 0, '15'),
(79, 'Serutan', 75, 0, '16'),
(80, 'Stabillo', 75, 0, '17'),
(81, 'File', 0, 0, '18'),
(82, 'Amplop', 81, 0, '19'),
(83, 'Ordner', 81, 1, '20'),
(84, 'Lainnya', 0, 0, '21'),
(85, 'Binder Clip', 84, 0, '22'),
(86, 'Cutter', 84, 0, '23'),
(87, 'Paper Clip', 84, 0, '24'),
(88, 'Gunting', 84, 0, '25'),
(89, 'Stapples', 84, 0, '26'),
(90, 'Tinta Refill', 84, 0, '27'),
(91, 'map', 81, 0, '28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) DEFAULT NULL,
  `product_category` varchar(100) DEFAULT NULL,
  `product_brand` int(11) DEFAULT NULL,
  `product_description` mediumtext,
  `product_price` varchar(100) NOT NULL,
  `product_unit` varchar(100) NOT NULL,
  `product_image` text,
  `positions` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data untuk tabel `product`
--

INSERT INTO `product` (`id`, `product_name`, `product_category`, `product_brand`, `product_description`, `product_price`, `product_unit`, `product_image`, `positions`) VALUES
(11, 'Amplop Putih Panjang 90', '82', 4, '', 'Rp. 14.500', 'Doos', '16-02-29_04-08-am_1M6l8_royal-90.jpg', '48'),
(12, 'Amplop Putih Panjang 104', '82', 4, '', 'Rp. 13.000', 'Doss', '16-02-29_04-10-am_xPLGZ_amplop-royal-104.jpg', '48'),
(13, 'Amplop Coklat ', '82', 5, 'Tersedia dalam ukuruan :\r\nA4, F4, Kabinet (Amplop Gaji), dan Q4', 'Rp. 30.000', 'Pack', '16-02-29_04-10-am_r2hj1_namas_amplot_coklat_map_isi_100.jpg', '48'),
(17, 'Ballpoint Standart AE7', '76', 6, '', 'Rp. 15.000', 'Lusin', '16-02-29_04-13-am_WKpNh_standar-ae7.jpg', '48'),
(18, 'Ballpoint Faster C6', '76', 7, '', 'Rp. 25.000', 'Lusin', '16-02-29_04-11-am_U60gx_faster.jpg', '48'),
(19, 'Buku Tulis BO', '69', 12, '', 'Rp. 2.200', 'Buah', '16-02-29_04-12-am_C7lX9_download.jpg', '48'),
(20, 'Buku Tulis Quarto 100 ', '68', 20, '', 'Rp. 7.000', 'Buah', '16-02-29_04-14-am_sy6nb_quarto.jpg', '48'),
(21, 'Buku Tulis Folio 100', '68', 20, '', 'Rp. 10.500', 'Buah', '16-02-29_04-14-am_fBJdM_folio.jpg', '48'),
(22, 'Buku Kwitansi Kecil', '67', 9, '', 'Rp. 2.200', 'Buah', '16-02-29_04-14-am_sTnWU_buku_kwitansi1.jpg', '48'),
(23, 'Buku Kwitansi Besar', '67', 9, '', 'Rp. 3.500', 'Buah', '16-02-29_04-15-am_XUTZY_buku_kwitansi.jpg', '48'),
(24, 'Box File Plastik ', '83', 10, '', 'Rp. 17.500', 'Buah', '16-02-29_04-17-am_wETcm_box-file.jpg', '48'),
(25, 'Binder Clip 105', '85', 8, '', 'Rp. 2.500', 'Lusin', '16-02-29_04-35-am_Fk85A_clip-105.jpg', '48'),
(26, 'Binder Clip 107', '85', 8, '', 'Rp. 3.000', 'Lusin', '16-02-29_04-42-am_YKxLa_clip-107.jpg', '48'),
(27, 'Binder Clip 111', '85', 8, '', 'Rp. 4.500', 'Lusin', '16-02-29_04-43-am_3wYGW_clip-111.jpg', '48'),
(28, 'Binder Clip 155', '85', 8, '', 'Rp. 5.500', 'Lusin', '16-02-29_04-53-am_ClxvF_clip-155.jpg', '48'),
(29, 'Binder Clip 200', '85', 8, '', 'Rp. 8.500', 'Lusin', '16-02-29_04-43-am_h4YTi_clip-200.jpg', '48'),
(30, 'Binder Clip 260', '85', 4, '', 'Rp. 13.000', 'Lusin', '16-02-29_04-44-am_vDmWs_clip-260.jpg', '48'),
(31, 'Clip Kecil No 3', '87', 8, '', 'Rp. 1.500', 'Doss', '16-02-29_04-46-am_62gMa_paper_klip.jpg', '48'),
(32, 'Clip Sedang No 1', '87', 8, '', 'Rp. 2.000', 'Doss', '16-02-29_04-47-am_1smj0_paper-01.jpg', '48'),
(33, 'Clip Besar No 5', '87', 8, '', 'Rp. 3.000', 'Doss', '16-02-29_04-54-am_JaID1_paper-05.jpg', '48'),
(34, 'Cutter Kecil', '86', 8, '', 'Rp. 5.500', 'Buah', '16-02-29_04-57-am_ycmH3_cutter-kecil.jpg', '48'),
(35, 'Cutter Besar', '86', 8, '', 'Rp. 11.000', 'Buah', '16-02-29_04-59-am_E623B_cutter-besar.jpg', '48'),
(36, 'Double Tape 1/4', '72', 22, '', 'Rp. 2.000', 'Roll', '16-02-29_05-10-am_V9KJe_double_1tape.jpg', '48'),
(37, 'Double Tape 1/2', '72', 22, '', 'Rp. 3.500', 'Roll', '16-02-29_05-11-am_9LBfJ_dobel_tape.jpg', '48'),
(38, 'Double Tape 1', '72', 22, '', 'Rp.6.000', 'Roll', '16-02-29_05-11-am_6tn7Q_dobel_tape.jpg', '48'),
(39, 'Garisan Plastik 30 ', '77', 23, '', 'Rp. 2.500', 'Buah', '16-02-29_05-26-am_y58ul_penggaris-30.jpg', '48'),
(40, 'Garisan Besi 30', '77', 11, '', 'Rp. 6.000', 'Buah', '16-02-29_05-28-am_jIVwN_garisan-besi.jpg', '48'),
(41, 'Gunting Sedang Bagus', '88', 24, '', 'Rp. 6.000', 'Buah', '16-02-29_05-29-am_jCRw0_Untitled-1.jpg', '48'),
(42, 'Gunting Sedang Biasa', '88', 24, '', 'Rp . 7.500', 'Buah', '16-02-29_05-32-am_rZQUw_Untitled-1.jpg', '48'),
(43, 'Isi Cutter Kecil', '86', 8, '', 'Rp. 2.000', 'Tube', '16-02-29_05-44-am_uDtF6_isi-cutter-kecil.jpg', '48'),
(44, 'Isi Cutter Besar', '86', 8, '', 'Rp. 4.500', 'Tube', '16-02-29_05-46-am_BvFAK_isi-cutter-besar.jpg', '48'),
(45, 'Continous Form 9 1/2 X 11 1 PLY NCR', '65', 13, 'Cont From 9 1/2 X 11 1 PLY NCR', 'Rp. 85.000', ' Box', '16-02-29_05-51-am_G0V6j_con_form.jpg', '48'),
(46, 'Continous Form 9 1/2 X 11 2 PLY NCR', '65', 13, 'Cont From 9 1/2 X 11 2 PLY NCR', 'Rp. 125.000', 'Box', '16-02-29_05-51-am_VMG0e_con_form.jpg', '48'),
(47, 'Continous Form 9 1/2 X 11 3 PLY NCR', '58', 4, 'Cont From 9 1/2 X 11 3 PLY NCR', 'Rp. 180.000', 'Box', '16-02-29_05-51-am_atIu9_con_form.jpg', '48'),
(48, 'Continous Form 9 1/2 X 11 4 PLY NCR', '58', 4, 'Cont From  9 1/2 X 11 4 PLY NCR', 'Rp. 130.000', 'Box', '16-02-29_05-52-am_C4MRu_con_form.jpg', '48'),
(49, 'Continous Form  9 1/2 X 11 5 PLY NCR', '58', 4, 'Cont From  9 1/2 X 11 4 PLY NCR', 'Rp. 160.000', 'Box', '16-02-29_05-52-am_YQoEi_con_form.jpg', '48'),
(50, 'Continous Form  14 7/6 11 1 PLY NCR', '65', 13, 'Continous Form  14 7/6 11 1 PLY NCR', 'Rp. 125.000', 'Box', '2015-12-18_cbc7c4a4e5f521f732b8c75edd1cdb78.jpg', '48'),
(51, 'Isi Staples Kecil', '89', 25, '', 'Rp. 2.500', 'Doss', '16-02-29_05-58-am_rKubq_is-stapples-kecil.jpg', '48'),
(52, 'Isi Staples Besar', '89', 25, '', 'Rp. 4.000', 'Doos', '16-02-29_06-02-am_GyQw0_stappless-besar.jpg', '48'),
(55, 'Kertas Fy Fax 210/216', '64', 14, '', 'Rp. 9.000', 'Roll', '16-02-29_06-04-am_4kdOn_fy-fax.jpg', '48'),
(54, 'Kertas Hvs A4/F4', '64', 14, '', 'Rp. 27.000', 'Rim', '16-02-29_06-05-am_rT0NO_natural-f4.jpg', '48'),
(56, 'Map Warna Warni', '91', 27, '', 'Rp. 17.000', 'Pack', '16-02-29_06-08-am_dJOUq_carry-file.jpg', '48'),
(57, 'Pensil 2B', '77', 26, '', 'Rp. 8.500', 'Isn', '16-02-29_06-09-am_uBSvc_pensil-2b.jpg', '48'),
(58, 'Plakban Coklat/Bening', '71', 8, '', 'Rp. 8.000', 'Roll', '16-02-29_06-12-am_Vsnjx_lakban-kuning.jpg', '48'),
(59, 'Stip/Penghapus Kecil', '77', 8, '', 'Rp. 2.000', 'Buah', '16-02-29_06-13-am_a8dQ4_eraser.jpg', '48'),
(60, 'Refill Tinta 100 ml', '90', 17, '', 'Rp. 25.000', 'Botol', '16-02-29_06-16-am_JPfeB_white_board_maker_refill.jpg', '48'),
(61, 'Stabilo ', '80', 8, '', 'Rp. 8.000', 'Buah', '16-02-29_06-17-am_xda5E_High_lighter.jpg', '48'),
(62, 'Ordner Bantex F4/Kw/F4 small', '83', 10, '', 'Rp. 160.000', 'Lusin', '16-02-29_06-20-am_cqREN_Ordner_Bantex_A4_1451_20130628160053.jpg', '48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text,
  `positions` int(3) DEFAULT NULL,
  `active` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data untuk tabel `slider`
--

INSERT INTO `slider` (`id`, `image`, `positions`, `active`) VALUES
(21, '2016-01-15_29558d62588edac9cd4dfcd8b9e51da9.jpg', 2, 1),
(4, '2015-12-28_bf3c4858365a47e593a17c78ef662f60.jpg', 4, 1),
(6, '2015-12-19_73f67f70b6dc02be3ef874d2773c3545.jpg', 3, 1),
(20, '2016-01-15_cb273b62de68b922b87d745cca7012f9.gif', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `web_config`
--

CREATE TABLE IF NOT EXISTS `web_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `val` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `web_config`
--

INSERT INTO `web_config` (`id`, `name`, `val`) VALUES
(1, 'web_name', 'ATK Bola Dunia'),
(2, 'web_title', 'ATK Bola Dunia'),
(3, 'web_description', 'Jual Beli Alat Tulis Kantor Terlengkap'),
(4, 'web_keywords', 'Bola Dunia, Alat Tulis Kantor, ATK, Perlengkapan Kantor, Alat Tulis, Perlengkapan, PD Bola Dunia, Grosir ITC Mangga Dua, Grosir ITC, Grosir Alat Tulis Kantor, Grosir Alat Tulis, Grosir Bola Dunia, Grosir Jakarta, ATK Jakarta'),
(5, 'web_email', 'anwarkuswadi15@gmail.com'),
(6, 'phone', '0216014075'),
(7, 'address', 'ITC Mangga Dua Lantai Dasar Blok E2 No. 11, \r\nJl. Mangga Dua Raya, \r\nPademangan, Kota Jkt Utara, \r\nDaerah Khusus Ibukota Jakarta \r\n14430'),
(8, 'web_logo', '2015-12-28_79582179793371df8ad8ccb9aad57470.jpg'),
(9, 'fax', '0216016729'),
(10, 'facebook', 'https://www.facebook.com/chen.a.lie.5'),
(11, 'twitter', 'https://twitter.com/?lang=en');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
